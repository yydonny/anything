

### Awesome

- [Modern Java Code Quality](https://github.com/palantir/gradle-baseline)

- [Java Binary IO and Manipulation](https://github.com/square/okio)

- [Bind Java thread to given core](https://github.com/OpenHFT/Java-Thread-Affinity)

- [Java Memory Analyzer](https://www.eclipse.org/mat/)

- [Resillience4J](https://github.com/resilience4j/resilience4j#8-talks)


### Data Structure & Algorithms

- [API Design for Heaps (aka Priority Queues)](http://typelevel.org/blog/2016/11/17/heaps.html)

- [Understanding Persistent Vector](https://hypirion.com/musings/understanding-persistent-vector-pt-1) [2](https://hypirion.com/musings/understanding-persistent-vector-pt-2) [3](https://hypirion.com/musings/understanding-persistent-vector-pt-3)
- [Understanding Clojure's Transients](https://hypirion.com/musings/understanding-clojure-transients)
- [Persistent Vector Performance Summarised](http://hypirion.com/musings/persistent-vector-performance-summarised)

- [Introduction to kd-trees](http://www.cs.umd.edu/class/spring2002/cmsc420-0401/pbasic.pdf)

- [K’th Largest Element in Unsorted Array](http://www.geeksforgeeks.org/kth-smallestlargest-element-unsorted-array/), [2](http://stackoverflow.com/questions/251781/how-to-find-the-kth-largest-element-in-an-unsorted-array)

- [给一个树T，一共n个节点，每个节点带有一个实数权值（可为负），找出以T的根节点为根节点的最大权重子树](http://bbs.saraba1st.com/2b/forum.php?mod=viewthread&tid=1210410&extra=page%3D2&mobile=2)


### Core Java

- [Non-blocking IO in Java](http://gee.cs.oswego.edu/dl/cpjslides/nio.pdf), [2](https://stackoverflow.com/questions/25099640/non-blocking-io-vs-async-io-and-implementation-in-java)

- [Java friendly Kotlin Code](https://android.jlelse.eu/writing-java-friendly-kotlin-code-c408b24fb4e)

- [Kotlin Internals](https://www.youtube.com/watch?v=Ta5wBJsC39s)


### Java Performance

- [Java command line options for JVM tuning](http://www.techpaste.com/2012/02/java-command-line-options-jvm-performance-improvement/)
- [codecentric Blog - Performance](https://blog.codecentric.de/en/category/performance-en/)

- [Java Z GC](https://hub.packtpub.com/getting-started-with-z-garbage-collectorzgc-in-java-11-tutorial/)


- JVM Tuning: read Cassandra jvm.options

- JVM performance flags
```
https://blog.codecentric.de/en/2012/07/useful-jvm-flags-part-1-jvm-types-and-compiler-modes/
https://blog.codecentric.de/en/2012/07/useful-jvm-flags-part-2-flag-categories-and-jit-compiler-diagnostics/
https://blog.codecentric.de/en/2012/07/useful-jvm-flags-part-3-printing-all-xx-flags-and-their-values/
https://blog.codecentric.de/en/2012/07/useful-jvm-flags-part-4-heap-tuning/
https://blog.codecentric.de/en/2012/08/useful-jvm-flags-part-5-young-generation-garbage-collection/
https://blog.codecentric.de/en/2013/01/useful-jvm-flags-part-6-throughput-collector/
https://blog.codecentric.de/en/2013/10/useful-jvm-flags-part-7-cms-collector/
https://blog.codecentric.de/en/2014/01/useful-jvm-flags-part-8-gc-logging/
```



### Docker

- [The Docker Ecosystem](https://www.digitalocean.com/community/tutorials/the-docker-ecosystem-an-introduction-to-common-components)

- [Remote Java Debugging With Docker](http://ptmccarthy.github.io/2014/07/24/remote-jmx-with-docker/)

- [Docker Storage Driver Tunning](http://www.projectatomic.io/blog/2015/06/notes-on-fedora-centos-and-docker-storage-drivers/) [2](http://stackoverflow.com/questions/37672018/clean-docker-environment-devicemapper)
- [Docker Tooling for JavaEE](https://www.youtube.com/watch?v=tq58w0DYmgc)
- [Docker DNS & Service Discovery with Consul and Registrator](http://artplustech.com/docker-consul-dns-registrator/)

- [Using Docker and Docker Compose for local Django development, replacing virtualenv](https://www.calazan.com/using-docker-and-docker-compose-for-local-django-development-replacing-virtualenv/)

- [Docker networking](https://stackoverflow.com/questions/24319662/from-inside-of-a-docker-container-how-do-i-connect-to-the-localhost-of-the-mach)

- [reverse engineering dockerfile from docker image](https://forums.docker.com/t/how-can-i-view-the-dockerfile-in-an-image/5687/3)

```
https://github.com/phusion/baseimage-docker
https://blog.phusion.nl/2015/01/20/docker-and-the-pid-1-zombie-reaping-problem/
```

- [Ignore the Hype: 5 Docker Misconceptions](http://blog.takipi.com/ignore-the-hype-5-docker-misconceptions-java-developers-should-consider/)



### Fork-Join Frameworks

```
http://homes.cs.washington.edu/~djg/teachingMaterials/grossmanSPAC_forkJoinFramework.html
http://faculty.ycp.edu/~dhovemey/spring2011/cs365/lecture/lecture18.html
http://www.javaworld.com/article/2078440/enterprise-java/java-tip-when-to-use-forkjoinpool-vs-executorservice.html
http://stackoverflow.com/questions/7926864/how-is-the-fork-join-framework-better-than-a-thread-pool
http://blog.takipi.com/forkjoin-framework-vs-parallel-streams-vs-executorservice-the-ultimate-benchmark/
```



### Frameworks


- [Spring JMS with ActiveMQ](http://shengwangi.blogspot.com/2014/10/spring-jms-with-activemq-helloworld-example-send.html)




### Microservices

- [API Gateways](https://itnext.io/using-api-gateways-to-facilitate-your-transition-from-monolith-to-microservices-c08fe3489237)

- [Lyft Envoy threading model](https://blog.envoyproxy.io/envoy-threading-model-a8d44b922310)

- Consul Tutorials [1](https://www.digitalocean.com/community/tutorials/an-introduction-to-using-consul-a-service-discovery-system-on-ubuntu-14-04) [2](https://www.digitalocean.com/community/tutorials/how-to-configure-consul-in-a-production-environment-on-ubuntu-14-04) [3](https://docs.docker.com/samples/library/consul/)


- [Adopting Microservices at Netflix: Lessons for Architectural Design](https://www.nginx.com/blog/microservices-at-netflix-architectural-best-practices/)

- [A Microscope on Microservices](https://medium.com/netflix-techblog/a-microscope-on-microservices-923b906103f4)
- [Microservices by Martin Fowler](https://martinfowler.com/microservices/)
- [Microservices by InfoQ](https://www.infoq.com/articles/microservices-intro)
- [基于微服务的软件架构模式](http://www.jianshu.com/p/546ef242b6a3)
- [Microservices is more than a buzzword](http://searchmicroservices.techtarget.com/feature/Microservices-is-more-than-a-buzzword)


### Messaging - Reliable delivery

```
- http://stackoverflow.com/questions/26640218/how-to-guarantee-jms-reliable-delivery
- http://wso2.com/library/articles/2013/01/jms-message-delivery-reliability-acknowledgement-patterns/
- http://www.atomikos.com/Publications/ReliableJmsWithTransactions
- http://abeykoon.blogspot.com/2013/04/get-jms-local-transactions-to-work-with.html
- http://camel.apache.org/guaranteed-delivery.html
- http://doc.akka.io/docs/akka/snapshot/contrib/reliable-proxy.html
- http://stackoverflow.com/questions/16812037/right-design-in-akka-message-delivery
- http://stackoverflow.com/questions/23100049/how-are-akka-messages-ordered-if-messages-are-not-reliab
- http://www.slideshare.net/bantonsson/real-world-akka-actor-recipes-javaone-2013
- http://devblog.consileon.pl/2014/04/15/reactive-ddd-with-akka/
- http://jdgoldie.github.io/blog/2015/01/07/reliable-delivery-in-akka-and-number-9
- http://krasserm.blogspot.com/2013/03/eventsourced-for-akka-high-level.html
- http://www.beingtheworst.com/episodes/16405-episode-29-acting-like-we-get-the-message
- http://blogs.rti.com/2014/08/14/replacing-zeromq-with-rti-connext-dds-in-an-actor-based-system/
```



