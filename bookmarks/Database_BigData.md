

### Relational

- [Full text search in milliseconds with PostgreSQL](https://blog.lateral.io/2015/05/full-text-search-in-milliseconds-with-postgresql/)

- [Postgres 10 declaritive partitioning](https://blog.timescale.com/scaling-partitioning-data-postgresql-10-explained-cd48a712a9a1)

- [SQL Index Performance](https://dba.stackexchange.com/questions/47462/index-for-date-column-for-range-query-in-oracle), [2](https://stackoverflow.com/questions/2292662/how-important-is-the-order-of-columns-in-indexes), [3](https://dba.stackexchange.com/questions/47462/index-for-date-column-for-range-query-in-oracle), [4](https://www.postgresql.org/docs/9.6/indexes-multicolumn.html)


- [SQL Index ordering](https://stackoverflow.com/questions/24315151/does-order-of-fields-of-multi-column-index-in-mysql-matter), [2](https://use-the-index-luke.com/sql/where-clause/the-equals-operator/concatenated-keys), [3](https://stackoverflow.com/questions/2292662/how-important-is-the-order-of-columns-in-indexes)

- [ORM is an anti-pattern](https://github.com/brettwooldridge/SansOrm/wiki/ORM-is-an-anti-pattern)


- [TiDB: A NewSQL database that supports Hybrid Transactional and Analytical Processing](https://pingcap.com/docs/stable/)

### HBase, Cassandra

- [HBase vs Cassandra](https://www.quora.com/How-does-HBase-write-performance-differ-from-write-performance-in-Cassandra-with-consistency-level-ALL)

- [Amy's Cassandra 2.1 tuning guide](https://tobert.github.io/pages/als-cassandra-21-tuning-guide.html)



### Hadoop

- [HDFS High Availability with Zookeeper](https://www.edureka.co/blog/how-to-set-up-hadoop-cluster-with-hdfs-high-availability/) [2](https://hadoop.apache.org/docs/r2.7.4/hadoop-project-dist/hadoop-hdfs/HDFSHighAvailabilityWithQJM.html)
- [Hadoop Installation the Definitive Guide](http://www.alexjf.net/blog/distributed-systems/hadoop-yarn-installation-definitive-guide/)
- [How to Plan and Configure YARN and MapReduce](https://zh.hortonworks.com/blog/how-to-plan-and-configure-yarn-in-hdp-2-0/)
- [Hadoop virtual memory config](http://stackoverflow.com/questions/29576679/hadoop-running-beyond-virtual-memory-limits-showing-huge-numbers) [2](http://stackoverflow.com/questions/21005643/container-is-running-beyond-memory-limits)
- [Best Practices for YARN Resource Management](https://mapr.com/blog/best-practices-yarn-resource-management/)
- [Tune Hadoop Cluster to get Maximum Performance](http://crazyadmins.com/tune-hadoop-cluster-to-get-maximum-performance-part-1/) [2](http://crazyadmins.com/tag/tuning-yarn-to-get-maximum-performance/)

```
http://ercoppa.github.io/HadoopInternals/HadoopArchitectureOverview.html
http://www.ibm.com/developerworks/library/bd-yarn-intro/
http://stackoverflow.com/questions/21005643/container-is-running-beyond-memory-limits
https://community.hortonworks.com/questions/57377/container-is-running-beyond-memory-limits.html
https://community.hortonworks.com/questions/17384/physical-memory-limits.html
https://community.hortonworks.com/questions/57377/container-is-running-beyond-memory-limits.html
https://discuss.elastic.co/t/unable-to-start-elasticsearch-yarn-container-is-running-beyond-virtual-memory-limits/26102
```


### MongoDB

- MongoDB `$elemMatch` [1](https://stackoverflow.com/questions/16511188/mongodb-does-not-use-indexes-for-exists-and-elemmatch) [2](https://stackoverflow.com/questions/10207448/mongodb-indexes-for-elemmatch) [3](https://stackoverflow.com/questions/14040562/how-to-search-in-array-of-object-in-mongodb)

- [Why You Should Never Use MongoDB](http://www.sarahmei.com/blog/2013/11/11/why-you-should-never-use-mongodb/)


```
https://slamdata.com/news-and-blog/2015/10/20/mongodb-and-the-shocking-case-of-the-missing-join-lookup/
https://www.linkedin.com/pulse/mongodb-32-now-powered-postgresql-john-de-goes
```

### Riak

```
- https://www.quora.com/What-are-possible-use-cases-for-Riak-HyperTable-or-MongoDB
- http://www.slideshare.net/argv0/riak-use-cases-dissecting-the-solutions-to-hard-problems
- http://basho.com/posts/technical/why-vector-clocks-are-hard/

```

### Google Dremel

```
http://stackoverflow.com/questions/6607552/what-is-googles-dremel-how-is-it-different-from-mapreduce
https://www.quora.com/How-will-Googles-Dremel-change-future-Hadoop-releases
```

