


### Maven Multi-Module Project

```
http://stackoverflow.com/questions/12223754/how-do-i-setup-maven-sub-projects-with-inter-dependencies
http://stackoverflow.com/questions/14694139/how-to-resolve-dependencies-between-modules-within-multi-module-project
http://stackoverflow.com/questions/1114026/maven-modules-building-a-single-specific-module
https://maven.apache.org/guides/mini/guide-multiple-modules.html
http://stackoverflow.com/questions/4150180/how-to-make-one-module-depends-on-another-module-artifact-in-maven-multi-modules
http://stackoverflow.com/questions/29712865/maven-cannot-resolve-dependency-for-module-in-same-multi-module-project
```


### Monad

```
http://scabl.blogspot.com/2013/02/monads-in-scala-1.html
http://adit.io/posts/2013-06-10-three-useful-monads.html
http://julien-truffaut.github.io/Monocle/
https://github.com/mpilquist/simulacrum/

```

- [A Schemer's Introduction to Monads](http://www.ccs.neu.edu/home/dherman/research/tutorials/monads-for-schemers.txt)
- [Functors, Applicatives, And Monads In Pictures](http://adit.io/posts/2013-04-17-functors,_applicatives,_and_monads_in_pictures.html)

- [Monad 1](https://www.youtube.com/watch?v=ZhuHCtR3xq8)
- [Monad 2](https://www.youtube.com/watch?v=Mw_Jnn_Y5iA)



### Scala

- [repo clean tool in Scala](https://github.com/rtyley/bfg-repo-cleaner)


```

http://jsuereth.com/scala/2011/02/18/2011-implicits-without-tax.html
http://eed3si9n.com/revisiting-implicits-without-import-tax
http://stackoverflow.com/questions/15568016/scala-object-cloning-copying-without-value-re-valuation

http://www.michael-noll.com/blog/2013/12/02/twitter-algebird-monoid-monad-for-large-scala-data-analytics/
https://www.quora.com/What-is-Twitters-interest-in-abstract-algebra-with-algebird
http://eed3si9n.com/learning-scalaz/

http://blog.michaelhamrah.com/2014/03/running-an-akka-cluster-with-docker-containers/
http://blog.michaelhamrah.com/2014/06/akka-clustering-with-sbt-docker-and-sbt-native-packager/

http://www.lightbend.com/activator/template/akka-docker-cluster

http://stackoverflow.com/questions/15568016/scala-object-cloning-copying-without-value-re-valuation

```



