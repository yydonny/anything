


## Stack Overflow

- [Find out shell script folder](https://stackoverflow.com/questions/242538/unix-shell-script-find-out-which-directory-the-script-file-resides)

- [Use Maven Shade to make conflicting library co-exist](https://vlkan.com/blog/post/2015/11/27/maven-protobuf/)

- [Groovy Script Include](https://stackoverflow.com/questions/9136328/including-a-groovy-script-in-another-groovy)



## To Read

```
https://www.allthingsdistributed.com/2015/07/under-the-hood-of-the-amazon-ec2-container-service.html
http://blog.fogcreek.com/we-spent-a-week-making-trello-boards-load-extremely-fast-heres-how-we-did-it/
http://blog.ninlabs.com/2013/12/software-engineering-the-next-50-years/
https://kerflyn.wordpress.com/2011/02/14/playing-with-scalas-pattern-matching/
https://www.infoq.com/articles/Succeeding-Dependency-Injection
http://blog.ploeh.dk/2014/03/10/solid-the-next-step-is-functional/
http://tomasp.net/blog/2015/library-frameworks/

```


- [Spring Boot Visualization](https://dzone.com/articles/lets-visualize-your-spring-boot-applications)

- [Writing Robust Bash Shell Scripts](https://www.davidpashley.com/articles/writing-robust-shell-scripts/)


- [Unyielding Greenlet](https://glyph.twistedmatrix.com/2014/02/unyielding.html)

- [Google Omega: flexible, scalable schedulers for large compute clusters](https://research.google.com/pubs/pub41684.html)

- [Concurrent Programming for Scalable Web Architectures](http://berb.github.io/diploma-thesis/community/index.html)

- [How to receive a million packets per second](https://blog.cloudflare.com/how-to-receive-a-million-packets/)

- [Thread Pools in NGINX Boost Performance 9x](https://www.nginx.com/blog/thread-pools-boost-performance-9x/)

- [How Autodesk Implemented Scalable Eventing over Mesos](http://highscalability.com/blog/2015/8/17/how-autodesk-implemented-scalable-eventing-over-mesos.html)

- [Podcasts - Event Sourcing, CQRS, distributed systems](http://www.beingtheworst.com/)

- [Rethinking PID 1](http://0pointer.de/blog/projects/systemd.html)

- [Measure Anything, Measure Everything](https://codeascraft.com/2011/02/15/measure-anything-measure-everything/)

- [Byte Sources: Introduction](https://blog.domenic.me/byte-sources-introduction/)

- [You're Missing the Point of Promises](https://gist.github.com/domenic/3889970)

- [An Ideal OpenStack Developer](https://blogs.gnome.org/markmc/2014/06/06/an-ideal-openstack-developer/)

- [Apache Kafka Hits 1.1 Trillion Messages Per Day](https://www.confluent.io/blog/apache-kafka-hits-1-1-trillion-messages-per-day-joins-the-4-comma-club/)

- [Cutting the Stinking Tauntaun and other Adventures in Software Archeology](https://analyzethedatanotthedrivel.org/2015/04/12/cutting-the-stinking-tauntaun-and-other-adventures-in-software-archeology/)

- [The Log: What every software engineer should know about real-time data's unifying abstraction](https://engineering.linkedin.com/distributed-systems/log-what-every-software-engineer-should-know-about-real-time-datas-unifying)


- [Python Cryptography](https://cryptography.io/en/latest/)
- [Java AES encryption example](https://gist.github.com/bricef/2436364/)
- [Java Transaction Design Strategies (eBook)](https://www.infoq.com/minibooks/JTDS)
- [Python args default value causing memory leak](https://lethain.com/digg-v4/)

- [Front End Security 2018](https://hackernoon.com/im-harvesting-credit-card-numbers-and-passwords-from-your-site-here-s-how-9a8cb347c5b5)




### 中文

- [Some curated notes](https://dirtysalt.github.io/)
- [Vimer的程序世界](http://www.vimer.cn/)
- [DBA Notes 闲思录](http://dbanotes.net/)
- [四火的唠叨](http://www.raychase.net/aboutme)
- [猫头鹰技术博客](http://mtydev.net/)
- [游戏开发随笔](http://zhuanlan.zhihu.com/gu-yu)
- [面向工资编程](http://zhuanlan.zhihu.com/auxten)
- [刘未鹏 | Mind Hacks](http://mindhacks.cn/)
- [Matrix67: The Aha Moments](http://www.matrix67.com/blog/)

- [移动端自适应方案](http://web.jobbole.com/83527/?from=groupmessage&isappinstalled=0)
- [实现秒杀的几个想法](http://blog.itpub.net/article.php?url=http://blog.itpub.net/29254281/viewspace-1800617/&from=groupmessage&isappinstalled=0)
- [代码审查“查”什么](http://mp.weixin.qq.com/s?__biz=MjM5MzA0OTkwMA==&mid=212920907&idx=1&sn=b5fa3af46fe6f9964bc6aa82c1eabb1e&scene=1&srcid=1002rfr5exU1p84b1bpx8ADj&from=groupmessage&isappinstalled=0#rd)
- [如何吊销 CNNIC 根证书](https://qdan.me/list/VRIio9hA0FujGWYP?utm_source=qdan.me#/)
- [你的Java代码对JIT编译友好么？](http://mp.weixin.qq.com/s?__biz=MjM5MDE0Mjc4MA==&mid=208946383&idx=1&sn=00f0e141dbfe7d8575207c1554705253&scene=1&srcid=azdZ9AMO9kIBxzZiByAM&from=groupmessage&isappinstalled=0#rd)
- [腾讯游戏如何使用Docker](http://mp.weixin.qq.com/s?__biz=MjM5MDE0Mjc4MA==&mid=208808439&idx=1&sn=04824ac4c161981d31ee0b02191f28bb&scene=21#wechat_redirect)
- [在 Ubuntu 12.04 上安装和配置邮件服务](http://www.vpsee.com/2012/06/install-mail-server-on-ubuntu-12-04/)
- [Bash 通过上下键更有效的查找历史命令](http://www.vpsee.com/2013/04/search-bash-history-using-the-up-and-down-arrows/)
- [Linux 多核下绑定进程到不同 CPU（CPU Affinity）](http://www.vpsee.com/2010/07/process-balancing-with-cpu-affinity/)
- [使用 Docker/LXC 迅速启动一个桌面系统](http://www.vpsee.com/2013/07/use-docker-and-lxc-to-build-a-desktop/)
- [在 CentOS 6.2 上安装 Puppet 配置管理工具](http://www.vpsee.com/2012/03/install-puppet-on-centos-6-2/)
- [程序员面试白皮书](https://zhuanlan.zhihu.com/p/19994258?columnSlug=donglaoshi)
- [高质量服务端之路(—)—Docker微服务架构实践](http://mtydev.net/?p=171)
- [高质量服务端之路(二)—Redis集群最佳实践](http://mtydev.net/?p=402)
- [傅里叶分析之掐死教程](http://zhuanlan.zhihu.com/wille/19763358)
- [快的打车架构实践](http://geek.csdn.net/news/detail/49447)
- [DockOne技术分享（三十九）：基于Docker和Git的持续集成工作流](http://dockone.io/article/916)
- [使用Docker在本地搭建hadoop，spark集群](http://dockone.io/article/944)
- [IBM和传统IT的沦落](http://blog.sina.com.cn/s/blog_53896b960102w8qo.html)
- [微信朋友圈技术之道：三个人的后台团队与每日十亿的发布量](http://mp.weixin.qq.com/s?__biz=MjM5MDE0Mjc4MA==&mid=401735823&idx=1&sn=7a4df58c22d9d3145e0692dd580f6a3d&scene=0#wechat_redirect)
- [关键业务系统的JVM启动参数推荐](http://calvin1978.blogcn.com/articles/jvmoption-2.html)
- [网络编程（七）：CAP原理推导和应用](http://zhuanlan.zhihu.com/auxten/20399316)
- [数据库sharding](http://blog.csdn.net/bluishglc/article/details/6161475)
- [关于大型网站技术演进的思考](http://www.cnblogs.com/sharpxiajun/default.html?page=2)
- [各大互联网公司架构演进之路汇总](http://www.hollischuang.com/archives/1036?hmsr=toutiao.io&utm_medium=toutiao.io&utm_source=toutiao.io)
- [最头疼的遗留系统该如何改造](http://mp.weixin.qq.com/s?__biz=MzA5Nzc4OTA1Mw==&mid=408409940&idx=1&sn=031cf076887a6e995eda05cf25249f05&scene=0#wechat_redirect)



