
Vagrant.configure(2) do |config|

  config.vm.box = "chef-bento/fedora23-vbox5-base"
  config.vm.box_check_update = false

  config.ssh.insert_key = false

  config.vm.network "private_network", type: "dhcp"

  config.vm.synced_folder "C:\\Developer\\Projects", "/projects"

  config.vm.provider "virtualbox" do |v|
    v.cpus = 2
    v.memory = "2048"
    v.gui = false
    # v.name = "try_disk"

    file_to_disk = File.realpath( "." ).to_s + "/disk.vdi"
    disk_size_mb = 12 * 1024

    unless File.exist?(file_to_disk)
       v.customize [
            'createhd',
            '--filename', file_to_disk,
            '--format', 'VDI',
            '--size', disk_size_mb]
       v.customize [
            'storageattach', :id,
            # Name of the storage controller. Mandatory. 
            # The list of the storage controllers currently attached to a VM 
            # can be obtained with VBoxManage showvminfo;
            '--storagectl', 'SATA Controller',
            '--port', 1,
            '--device', 0,
            '--type', 'hdd',
            '--medium', file_to_disk]
    end
  end

  $make_partition = <<-SHELL
set -e
set -x

if [[ -f /etc/vagrant_provisioned_date ]]; then
   exit 0
fi

sudo fdisk -u /dev/sdb <<EOF
n
p
1

+500M
n
p
2


w
EOF

mkfs.ext4 /dev/sdb1
mkfs.ext4 /dev/sdb2
mkdir -p /{data,extra}
mount -t ext4 /dev/sdb1 /data
mount -t ext4 /dev/sdb2 /extra

date > /etc/vagrant_provisioned_date
  SHELL


end












