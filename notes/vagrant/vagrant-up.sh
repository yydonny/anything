
# ===========================================================================
# Install VirtualBox

curl -s https://www.virtualbox.org/download/oracle_vbox_2016.asc | apt-key add -
echo 'deb http://download.virtualbox.org/virtualbox/debian xenial contrib
' > /etc/apt/sources.list.d/virtualbox.list

sudo apt-get update
sudo apt-get install virtualbox-5.1

sudo apt-get install dkms
apt-get install vagrant

sudo /sbin/vboxconfig
# This system is not currently set up to build kernel modules (system extensions).
# Running the following commands should set the system up correctly:

#   apt-get install linux-headers-4.10.0-24-generic
# (The last command may fail if your system is not fully updated.)
#   apt-get install linux-headers-generic

# There were problems setting up VirtualBox.  To re-start the set-up process, run
#   /sbin/vboxconfig
# as root.

# ===========================================================================
# install Vagrant

apt-get install vagrant
apt-get remove vagrant # only keep dependencies

# download latest version
wget https://releases.hashicorp.com/vagrant/2.0.0/vagrant_2.0.0_x86_64.deb
dpkg -i vagrant_2.0.0_x86_64.deb



