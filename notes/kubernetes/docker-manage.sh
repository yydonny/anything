

sudo service docker start
sudo gpasswd -a ubuntu docker

# https://www.digitalocean.com/community/tutorials/how-to-remove-docker-images-containers-and-volumes
docker rm $(docker ps -a -f status=exited -q)

# ---------------------------------------------------------------------------

# Change the storage path of the docker daemon
# https://docs.docker.com/engine/reference/commandline/dockerd/
# https://docs.docker.com/engine/admin/#configure-the-docker-daemon
# https://sanenthusiast.com/change-default-image-container-location-docker/
sudo -s
cd /etc/docker/
cat >> daemon.json << __done
{
  "graph": "/data2/docker"
}
__done

service docker stop
cp -r -p /var/lib/docker /data2/

cd /var/lib
rm -rf docker
ln -s /data2/docker docker

service docker start

# more on this
# http://www.projectatomic.io/blog/2015/06/notes-on-fedora-centos-and-docker-storage-drivers/
# http://stackoverflow.com/questions/37672018/clean-docker-environment-devicemapper

# ---------------------------------------------------------------------------
# Docker compose example
docker build -t friendlyhello .

docker-compose up -d

#  docker run -d -p 4000:80 friendlyhello

# ---------------------------------------------------------------------------

docker run -it --rm \
  -v `pwd`:/tmp/$(basename `pwd`) \
  -w /tmp/$(basename `pwd`) \
  maven:3 mvn package





# https://docs.docker.com/engine/reference/run/#foreground




docker run --name='activemq' -it --rm \
    -e 'ACTIVEMQ_MIN_MEMORY=512' -e 'ACTIVEMQ_MAX_MEMORY=2048' \
    -p 8161:8161 -p 10086:61616 -p 61613:61613 \
    webcenter/activemq:latest




