
# The following works with Docker 1.12 & Kubernetes 1.8, using KubeAdmin
# may also works for Docker 1.13 and up
# https://kubernetes.io/docs/setup/independent/create-cluster-kubeadm/

# =================================================================
# prepare (all nodes)


# As of release Kubernetes 1.8.0, kubelet will not work with enabled swap.
# https://github.com/kubernetes/kubernetes/issues/53333#issuecomment-333587943
# https://serverfault.com/questions/684771/best-way-to-disable-swap-in-linux
cat /proc/swaps
swapoff -a
vim /etc/fstab # Remove any matching reference 
reboot

# Set /proc/sys/net/bridge/bridge-nf-call-iptables to 1 to pass bridged IPv4 traffic to iptables’ chains.
# This is a requirement for CNI plugins to work, for more information please see here.
sysctl net.bridge.bridge-nf-call-iptables=1

# =================================================================
# install (all nodes)
# https://kubernetes.io/docs/setup/independent/install-kubeadm/

# set up package repository
apt-get install -y apt-transport-https
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -
echo "deb http://apt.kubernetes.io/ kubernetes-xenial main" > /etc/apt/sources.list.d/kubernetes.list

apt-get update

# install docker, kubernetes
apt install ebtables ethtool
apt-get install -y docker.io

apt-get install -y kubelet kubeadm kubernetes-cni

# =================================================================
# start up (master node)

# find out intranet address
ip addr

host_address=10.88.229.129
kubeadm init --pod-network-cidr=10.244.0.0/16 --apiserver-advertise-address=$host_address

  # --skip-preflight-checks
  # --kubernetes-version stable-1.8

# [apiclient] All control plane components are healthy after 15.538809 seconds
# [uploadconfig] Storing the configuration used in ConfigMap "kubeadm-config" in the "kube-system" Namespace
# [markmaster] Will mark node nadesico as master by adding a label and a taint
# [markmaster] Master nadesico tainted and labelled with key/value: node-role.kubernetes.io/master=""
# [bootstraptoken] Using token: 21142a.266764e65f978b12
# [bootstraptoken] Configured RBAC rules to allow Node Bootstrap tokens to post CSRs in order for nodes to get long term certificate credentials
# [bootstraptoken] Configured RBAC rules to allow the csrapprover controller automatically approve CSRs from a Node Bootstrap Token
# [bootstraptoken] Configured RBAC rules to allow certificate rotation for all node client certificates in the cluster
# [bootstraptoken] Creating the "cluster-info" ConfigMap in the "kube-public" namespace
# [addons] Applied essential addon: kube-dns
# [addons] Applied essential addon: kube-proxy

# Your Kubernetes master has initialized successfully!

# To start using your cluster, you need to run (as a regular user):

#   mkdir -p $HOME/.kube
#   sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
#   sudo chown $(id -u):$(id -g) $HOME/.kube/config

# You should now deploy a pod network to the cluster.
# Run "kubectl apply -f [podnetwork].yaml" with one of the options listed at:
#   http://kubernetes.io/docs/admin/addons/

# You can now join any number of machines by running the following on each node
# as root:

#   kubeadm join --token 21142a.266764e65f978b12 10.88.229.129:6443 --discovery-token-ca-cert-hash sha256:6a525b441790320684c3abf3f96af450be436233dccf16e31019c182                                                                                                      9d007344

# =================================================================
# Configure Flannel (master node)

useradd yangyd
mkdir /home/yangyd/
chown yangyd:yangyd /home/yangyd
echo 'yangyd ALL = NOPASSWD: ALL
yangyd ALL=(ALL) NOPASSWD:ALL
' > /etc/sudoers.d/yangyd

su yangyd
cd
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config

kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/v0.9.0/Documentation/kube-flannel.yml

# By default, your cluster will not schedule pods on the master for security reasons.
# If you want to be able to schedule pods on the master, e.g. for a single-machine Kubernetes cluster for development, run:
kubectl taint nodes --all node-role.kubernetes.io/master-


# =================================================================
# Setup worker nodes
# The token is used for mutual authentication between the master and the joining nodes. The token included here is secret, keep it safe — anyone with this token can add authenticated nodes to your cluster.
# kubeadm join --token 44ea80.28139aa973687670 10.88.229.129:6443 --discovery-token-ca-cert-hash sha256:428929959ed70745adfdd96d86cc29d927f554af23b4dcde79fe29916c4e8100

# =================================================================
# Ready to go

kubectl get all --namespace=kube-system

# deploy some image
kubectl run guids --image=alexellis2/guid-service:latest --port 9000
kubectl get pods

# Launch a full-fledged microservice ochestration
blueprint=https://raw.githubusercontent.com/microservices-demo/microservices-demo/master/deploy/kubernetes/complete-demo.yaml
app_namespace=sock-shop

kubectl create namespace $app_namespace

kubectl apply -n $app_namespace -f $blueprint
kubectl get pods -n $app_namespace
kubectl -n $app_namespace get svc front-end
# NAME        TYPE       CLUSTER-IP     EXTERNAL-IP   PORT(S)        AGE
# front-end   NodePort   10.99.244.84   <none>        80:30001/TCP   1h

# Visualizer
# https://www.weave.works/docs/scope/latest/installing/#k8s-standalone
kubectl apply --namespace $app_namespace -f "https://cloud.weave.works/k8s/scope.yaml?k8s-version=$(kubectl version | base64 | tr -d '\n')"

# Visualizer only listen to local port
# setup a local reverse proxy
# make sure websocket is supported in nginx.conf
sudo -s
apt-get install nginx
nano /etc/nginx/nginx.conf
systemctl restart nginx

kubectl port-forward -n $app_namespace "$(kubectl get -n $app_namespace pod --selector=weave-scope-component=app -o jsonpath='{.items..metadata.name}')" 4040:4040
# Forwarding from 127.0.0.1:4040 -> 4040
# how to forward port to docker bridge network?


