
### ZGC

ZGC is nice if you are doing things like web servers with large heaps and many CPU cores. ZGC is not nice if you on a single core box with limited memory doing batch processing.

- On a single core box? Serial collector, every time.
- Care about throughput? Parallel collector
- Care about response time? ZGC or G1GC depending on how much of a performance hit you want to take or how big your heap is and the overhead you care about.
- Have a smallish heap (less that 6gb) - Parallel collector all the way.

Generally, the lower the latency targets, the more overhead. ZGC will have the largest negative impact on throughput with stellar pause times.

As for the case you listed, with that small of a heap I'd go with the parallel collector (assuming a multi CPU setup). With a single CPU, it should always be the serial collector.



### Misc

JVM的类加载是lazy的。除非真正调用到该类对象的方法（包括new），否则就不会加载那个类。仅仅通过import那个类，甚至是该类来声明一个变量，并不会导致加载。这是很多运行时 class not found 的原因所在。

在使用增量构建时偶尔会遇到`java.lang.IncompatibleClassChangeErrors`，这是因为某些改动导致了新编译的类接口和以前编译类所调用的不兼容。Clear之后重编译即可.

The default retention policy for annotation is in class, but not in runtime. This means the __class loader may strip them off while loading__ class bytecodes.

`enum` member is a way to make a variable constant in Java.

**Boxing and unboxing can dramatically hurt performance**. That's why Java includes primitive containers (Atomic, Optional, Stream)

`ArrayList` can be much more efficient in a parallel stream than `LinkedList`, because it can be split without traversal.

### Weak Hashmap

A Hashmap implementation that uses weak reference for its __keys__, so that it can be effectively GCed if the key is not used anywhere else.

The main use for WeakHashMap is when you have mappings which you want to disappear when their keys disappear. A cache is the reverse---you have mappings which you want to disappear when their values disappear.

For a cache, what you want is a `Map<K,SoftReference<V>>`. A SoftReference will be garbage-collected when memory gets tight. (Contrast this with a WeakReference, which may be cleared as soon as there is no longer a hard reference to its referent.) You want your references to be soft in a cache (at least in one where key-value mappings don't go stale), since then there is a chance that your values will still be in the cache if you look for them later. If the references were weak instead, your values would be gc'd right away, defeating the purpose of caching.

(From [this Answer](https://stackoverflow.com/questions/1802809/javas-weakhashmap-and-caching-why-is-it-referencing-the-keys-not-the-values/1803213#1803213))


### Hash map changes in java 8

[Secondary Hash](https://dzone.com/articles/hashmap-performance)

## Java Collection framework revisit

- TreeMap: red-black tree, guaranteed log(n) lookup and insertion time.
- LinkedList: implements the Deque interface
- Stack and Queue: Queue is interface but Stack is concrete class. AbstractStack is the interface.


## Class file and Bytecode

### - Loading and Linking

1. The first step is read the binary stream of class file into memory, and create a `Class` object for it. Only basic check is done in this step.
2. Then the classes must be linked together. Linking breaks down into three steps: **verification, preparation, and resolution**,

- **Verification** checks the sanity of Bytecode. All check are static, including all the symbols, type constraints, access modifier, initialization and potential stack overflow. These checkes are done load time so that runtime check can be simplified for better performance.
- **Preparation** step allocates memory for classes all the static fields (but not initialize them)
- **Resolution** step resolves all references in the class and if necessary, start loading process of any new types thats not already loaded. After this step the `Class` object becomes fully functional.

After all the classes are loaded and resolved, JVM initializes the original class it is asked to load (e.g. the main class). This is when static initialization block gets executed.

Loading and Linking are done by Classloaders.

### - Classloaders

- Bootstrap loader: this is part of the JVM and it loads basic classes of JDK like rt.jar. Bootstrap loader is usually written in native language and doesn't perform verification.

- Extention loader: this one loads extention jars configured in the JVM installation

- Applicaiton loader, or System loader: the standard loader that loads all application code.

- Custom loader: implemented by application by extending the Classloader interface.

### - Method Handle

Method handles are type safe way of invoking a method dynamically in the runtime.

```java
MethodType mType = MethodType.methodType(int.class, String.class, String.class);
MethodHandle handle = MethodHandles.lookup().findVirtual("".getClass(), "compareTo", mType);
int compare = handle.invokeExact();
```



### - Examine class files: `javap`

`javap` can be used to list Bytecodes of a class, and also disassemble it.

### - Constant pool

Constant pool is an area contains shortcuts to other elements in classfile, like symbol table in C.

Contents of the pool includes: Class object, method and field reference, primitive values, **String**.

There are also dynamic entries in the constant pool: method handle and  `invokeDynamic`


### invokeDynamic

`invokeDynamic` is an opcode introduced in Java 7. It instruct the JVM to only resolve the exact method to invoke until runtime, which means no compile or link time resolution.

`invokeDynamic` use a *bootstrap method* to find the actual method. The bootstrap method returns a `CallSite` object, which holds a MethodHandle of the actual method.

`CallSite` objects can be relinked in the runtime, and can potentially be optimized by JIT.


## Concurrency and Multi-Threading

### `synchronized`

- Only objects can be synchronized (including the Class object for static methods)
- `synchronized` keyword can't be part of interface
- locks are *reentrant*.
- The memory representation of the object being locked is `synchronized` in different threads it is being accessed.
- The thread holding the lock gets a `synchronized` view of the object before the cretical acea being executed.
- Locks are reentrant: if a thread enconters a critical area guarded by the lock it holds, it is allowed to proceed.

### Thread states in java

When a thread is removed from scheduling, it could be in three states.

- blocked: when a thread is trying to acquire a **lock** or waiting for IO event to happen.
- waiting: when a thread is waiting for some **condition variable**. the Lock associated by the condition variable is released during the waiting.
- timed waiting: certain method will put thread in waiting for some specified time instead of condition variables. Methods with timeout include
Thread.sleep and the timed versions of `Object.wait Thread.join Lock.tryLock, and Condition.await`.

### `volatile`

- the value of a `volatile` variable if always read from main memory.
- the write to the variable is always flushed to main memory *before the write instruction completes*.
- there's no synchronization between reads and writes.

### Immutability

- Objects that either have no state or only have `final` fields.
- All `final` fields have to be immutable themselves
- Can never be in an inconsistent state
- Builder pattern

### Java memory model

- **Happens Before** - one blocks of code completes fully before the other can start
- **Synchronizes With** - an operation synchronizes its view of an object *with the main memory* before proceeding.

- An unlock operation of a monitor *synchronizes with* later lock operation.
- A write to volatile variable *synchronizes with* later reads.
- A *synchronizes with* B means A *happens before* B.
- Within one thread, happens-before is same as logical execution order.


### Atomic classes

- Similar semantics as `volatile` but more powerful (atomic compare and update)
- Utilizing hardware features to be lock-free


### Locks in `java.util.concurrent.lock`

- flexibility: read-write locks, lock and unlock can be in different methods
- Lock free `tryLock()`, and acquire with a timeout.
- Locks with more than one condition variables.

### `ConcurrentHashMap`

- lock is maintained on bucket basis, instead locking the whole hash table.
- lock-free reads

### `CopyOnWriteArrayList`

- all mutations create copy of the backing array so existing *iterators* won't be affected.

### `BlockingQueue`

- `put()` blocks when queue is full, while `take()` blocks when it is empty.
- can have timeouts on put and take

### `TransferQueue`

- Similar to `BlockingQueue`, but has an additional `transfer()` operation.
- If there's receiver thread waiting, transfer immediately pass the item to the receiver. Otherwise it blocks.
- Can use to implement *back-pressure* pattern.


### Fork/Join framework

- Automatic scheduling of task on a special thread pool.
- Tasks has to be able to be splitted into smaller tasks. Best suit of devide and counquer problem.
- Instead of recursion, call `fork()` to spawn execution of subtask, and then `join()` with their results.
- Because tasks take different sizes, the execution time can vary vastly. FockJoinPool will reassign tasks from busy thread to idle thread dynamically. This is called **work-stealing**.

```java
class Task extends RecursiveTask<Integer> {
  final int size;
  Task(int n) { this.n = n; }

  @Override
  Integer compute() {
    if (n <= 2)
      return basicCase(n);
    Task sub = new Task(n / 2);
    sub.fork(); // spawn parallel execution of the sub task
    return merge(sub.join()); // blocks until t1.compute() is done
  }

  Integer merge(int n) {
    // some merge work
  }

  Integer basicCase(int n) {
  }
}
```

- Example of parallel merge sort

Similar to classic merge sort, but instead of recursion, create subtasks of it self and call `invokeAll()`. Notice how all task instances shares a same backing array.

```java
class SortTask extends RecursiveAction {
  static final int THRESHOLD = 1000;

  final long[] array; final int lo, hi;
  SortTask(long[] array, int lo, int hi) {
    this.array = array; this.lo = lo; this.hi = hi;
  }

  protected void compute() {
    if (hi - lo < THRESHOLD)
      naiveSort(lo, hi);
    else {
      int mid = (lo + hi) >>> 1;
      invokeAll(new SortTask(array, lo, mid), new SortTask(array, mid, hi));
      merge(lo, mid, hi);
    }
  }

  void naiveSort(int lo, int hi) {
    Arrays.sort(array, lo, hi);
  }

  // The merge step
  // In place merge, only use n/2 of auxilliary space
  void merge(int lo, int mid, int hi) {
    long[] buf = Arrays.copyOfRange(array, lo, mid);
    for (int i = 0, j = lo, k = mid; i < buf.length; j++)
      array[j] = (k == hi || buf[i] < array[k]) ? buf[i++] : array[k++];
  }
}

```


## `java.util.concurrent.lock` in Detail

java.util.concurrent.lock provides a framework for locking and waiting for conditions that permits much greater flexibility.

The Lock interface supports locking disciplines that differ in semantics (reentrant, fairness, etc), and that can be used in non-block-structured contexts including hand-over-hand and lock reordering algorithms. The main implementation is `ReentrantLock`.

The `ReadWriteLock` interface similarly defines locks that may be shared among readers but are exclusive to writers. Only a single implementation, ReentrantReadWriteLock, is provided.

The `Condition` interface describes __condition variables__ that may be associated with Locks. These are similar to the implicit monitors accessed using `Object.wait`, but offer extended capabilities. In particular, multiple Condition objects may be associated with a single Lock.

- ReentrantLock

A reentrant mutual exclusion Lock with the same basic behavior and semantics as the implicit monitor lock accessed using synchronized methods and statements, but with extended capabilities.

The constructor for this class accepts an optional `fairness` parameter. When set true, under contention, locks favor granting access to the longest-waiting thread. Otherwise this lock does not guarantee any particular access order.

Programs using fair locks accessed by many threads may display lower overall throughput (i.e., are slower; often much slower) than those using the default setting, but have smaller variances in times to obtain locks and guarantee lack of starvation. Note however, that fairness of locks does not guarantee fairness of thread scheduling.

- ReadWriteLock

A ReadWriteLock maintains a pair of associated locks, one for read-only operations and one for writing. The read lock may be held simultaneously by multiple reader threads, so long as there are no writers. The write lock is exclusive.


