

# Algorithm Notes


## 特定子列问题

1. 低买高卖 [maximum difference between elements](https://www.geeksforgeeks.org/maximum-difference-between-two-elements/)
2. 最长非重复子串 [longest non-repeating character sequence](https://www.geeksforgeeks.org/length-of-the-longest-substring-without-repeating-characters/)
3. 最大子列 [maximum subarray](https://www.geeksforgeeks.org/largest-sum-contiguous-subarray/)
4. 特定求和子列 subarray that sums to specific number

前3个问题都可以在线性时间解决。

1)所求的子列两端必然是局部最小和最大，只需单次扫描，追踪所有遇到的局部最小以及从该最小值开始所见的最大差即可。

2)因为子串不会包含已经见过的字符，所以单次扫描，追踪每个所见字符的最后位置，一旦遇到重复，则从重复字符前一次出现位置开始，左边的部分都不需要再考虑（之前的最长候选也记录下来）

3)在算法导论中讨论过并给出了递归的分治解法，但实际上是可以在线性时间内解决的。最大和子列意味着要包括尽可能多的正数以及尽可能少的负数。具体说，在扫描过程中从起始位置求和，并记录和的最大值（及位置），但一旦发现和变为负数则抛弃之前的部分（因为包含这一部分子列只会降低和的值，而之前的最大和已经被记录下来）并**将所追踪的局部和清零重新继续**。这点很关键，即*不*包含之前的部分比包含可以得到更大的和值（负数 < 0）。

这三个问题的共同点是，要寻找的子列都存在某些特征，并且可以再一次扫描的过程中追踪更新，使得我们无需比较所有可能的子列。

- 一次扫描
- 追踪并更新部分结果
- 有明确的抛弃条件（动态规划）


第四个问题似乎没有以上特征。

思路一，如果可以无视次序的话，分成正数和负数两部分，从正数中寻找求和大于等于目标值的组合，然后从负数部分尝试加入元素来寻找结果。可以缓存出负数部分的所有可能组合，之后依次扫描即可。

思路二，对半分治的递归方法，如果元素次序不能改变的话。需要扫描跨越中间元素的所有子列，~~但这部分可以在线性时间完成（记录并更新已扫描部分的和）。因为每次递归涉及到一次线性时间的处理，因此和归并排序类似，递归方法的复杂度是O(nlogn)，好于简单穷举(n^2)。~~

扫描所有跨越中间的子列依然需要n^2时间，因为没有抛弃条件可用。分治应当也是n^2时间


以下是最大和子列的一个Java解法

```java
public class Solution {
    private static int[] solution(int[] input) {
        if (input == null || input.length == 0) {
            throw new IllegalArgumentException();
        }
        int currSum = 0;
        int currStart = 0;
        int maxSum = 0;
        int maxLen = 0;
        int maxStart = 0;
        
        for (int i = 0; i < input.length; i++) {
            int curr = input[i];
            currSum += curr;
            if (currSum < 0) {
                currSum = 0;
                currStart = i + 1;
            } else if (currSum > maxSum) {
                maxSum = currSum;
                maxStart = currStart;
                maxLen = i - currStart + 1;
            }
        }
        return new int[]{maxSum, maxStart, maxLen};
    }
    
    public static void main(String args[]) throws Exception {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            String line1 = reader.readLine();
            String[] numbers = line1.split(" ");
            int[] input = new int[numbers.length];
            for (int i = 0; i < numbers.length; i++) {
                input[i] = Integer.parseInt(numbers[i]);
            }
            System.out.println(Arrays.toString(solution(input)));
        }
    }
}
```

### 类似问题

- [Maximum subarray with certain difference](https://www.hackerrank.com/challenges/picking-numbers/problem)


