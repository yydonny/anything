
## New Notes

- columns must be declared before inserting
- insert will replace existing row with same primary key.
- non-primary columns have time stamp associated, and can be set with a TTL.
- columns can be primitive types as well as collection types. nesting collection type has to be made frozen (immutable).
- search can only be conducted on indexed columns. because of the storage model of cassandra, queries using secondary index inevitably involves scanning multiple nodes, and is significantly more expansive than using primary key.

- performance of secondary index is particularly bad when:
  - column has big cardinality (many distinct values)
  - column has very small cardinality (many repeated values)
  - frequently updated columns.

- in general, denormalized table design or materialized views are prefered to using secondary indexes.





---


## Old Notes


### CAP Theorem

- *consistency* - All database clients will read the same value for the same query, even given con current updates.
- *Availability* - All database clients will always be able to read and write data.
- *Partition tolerance* - The database can be split into multiple machines; it can continue functioning in the face of network segmentation breaks.

### Tuneable Consistency

- Strict consistency

It requires that any read will always return the most recently written
value. 

- Casual consistency

It means that writes that are potentially related must be read in sequence. If two different, unrelated operations suddenly write to the same field, then those writes are inferred not to be causally related. But if one write occurs after another, we might infer that they are causally related. Causal consistency dictates that causal writes must be read in sequence.

- Eventual consistency

### CAP Choice

Dynamo and Cassandra choose to be always writable, opting to defer the complexity of reconciliation to read operations.

Cassandra is optimized for excellent throughput on writes.

Many of the early production deployments of Cassandra involve storing user activity updates, social network usage, recommendations/reviews, and application statistics. These are strong use cases for Cassandra because they involve lots of writing with less predictable read operations, and because updates can occur unevenly with sudden spikes.


### Data Model

Cassandra’s data model can be described as a **partitioned row store**, in which data is stored in sparse multidimensional hashtables. 

"partitioned" means that each row has a unique key which makes its data accessible, and the keys are used to distribute the rows across multiple data stores.

Cassandra stores data in a multidimensional, sorted hash table. As data is stored in each column, it is stored as a separate entry in the hash table. Column values are stored according to a consistent sort order, omitting columns that are not populated.


Cassandra Query Language (CQL) provides a way to define schema via a syntax similar to the Structured Query Language (SQL) familiar to those coming from a relational background. 

Concepts:

- row and row key
- keyspace contains tables
- table
- wide row: a row that has lots of columns
- cluster, container of keyspaces


A wide row is typically divided into partitions. Cassandra uses a special primary key called **composite key** ot represent wide rows. 

A composite key consists of one partition key, and an optional **set of clustering columns**.

The outermost structure in Cassandra is the **cluster**, sometimes called the **ring**, because Cassandra assigns data to nodes in the cluster by arranging them in a ring.

Clusters are containers of **keyspaces**.

Table is order collection of rows.

Rows are order collections of columns.

Values in each column are versioned by the timestamp.



In Cassandra you don’t start with the data model; you start with the query model. Instead of modeling the data first and then writing queries, with Cassandra you *model the queries and let the data be organized around them.* Think of the most common query paths your application will use, and then create the tables that you need to support them. 



Cassandra tables are each stored in separate files on disk, it’s important to keep related columns defined together in the same table. 

A key goal is to minimize the number of partitions that must be searched in a given query. Because **the partition is a unit of storage that does not get divided across nodes**, a query that searches a single partition will typically yield the best performance.

The sort order available on queries is fixed, and is determined entirely by the selection of clustering columns.


------------




## Cassandra Data Modelling

- Primary key, partition key, and clustering

Every record has a primary key. Primary key can be **simple** or **compound**.

Either way, there has to be a **partition key**, for simple primary key, it is the . For compound primary key, the first column in the declaration becomes the partition key. It is also possible to explicitly define multiple columns as partition key. The rest of the columns in the primary key become **clustering columns**.

> The Partition Key is responsible for data distribution across your nodes.
> The Clustering Key is responsible for data sorting within the partition.

Partition key only controls data locality. It alone doesn't uniquely identify one row in the table.

All the rows sharing the same partition key (called a partition ?) are stored on the same physical node, _even across multiple table_. When data is inserted into the cluster, the first step is to apply a hash function to the partition key. The output is used to determine what node (and replicas) will get the data.

The partition process is deterministic, so no search is needed when query with given partition key.

The clustering column specifies the order that the data is arranged __inside the partition__. When CLUSTERING ORDER BY is used in time series data models, we can quickly access the last N items inserted.


Cassandra __orders its partitions__ by the *hashed value of the partition key*. Note that this order is maintained throughout the cluster, not just on a single node. Therefore, results for an unbound query (not recommended to be run in a multi-node configuration) will be ordered by the hashed value of the partition key, regardless of the number of nodes in the cluster.



_references_
- stackoverflow [1](https://stackoverflow.com/questions/24949676/difference-between-partition-key-composite-key-and-clustering-key-in-cassandra) [2](https://stackoverflow.com/questions/46340633/cassandra-is-partition-key-also-used-in-clustering)_
- [Primary Keys in CQL](http://thelastpickle.com/blog/2013/01/11/primary-keys-in-cql.html)
- 













