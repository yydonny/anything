
# implementation

- ADT
  - Linked List
  - Graphs

- Algorithms
  - Flatten nested list
  - DFS
  - BFS

# Algorithms

## Fundamental

- XOR 操作服从交换律和结合律。

- Hash map
  - hash function, collision
  - underlying structure: single array, buckets or search tree
- ArrayList
  - O(1) random access
  - Capacity grow stratagy: double and copy, takes O(n) time but amortized to O(1)

## Linked List

- Singly, Doubly Linked List
- Basic Operations
  - tracking the head (and the tail) node
  - traversing
  - insert and delete

- Problems
  - Find the mth to the last element
  - Flatten a nested list (each node might point to another list)
  - Cyclic detection


## Trees

### Binary tree

- Positioning, Array representation
- Height and element count
- Full and Complete
- Traversal
  - in-order
  - post-order
  - pre-order

### Binary Search Tree

- BST property
- Balance
- Red-Black tree

### Heap

- The heap property (max heap): if P is a parent node of C, then the key of P is greater than or equal to the key of C. (P >= C)
- The heap is one maximally efficient implementation of an abstract data type called a priority queue.
- A common implementation of a heap is the binary heap, which is a **complete binary tree**.

- there is no implied ordering between siblings or cousins and no implied sequence for an in-order traversal (as there would be in a BST)

- Binary heaps may be represented in a very space-efficient way using an array alone. The children of the node at position n would be at positions 2n + 1 and 2n + 2 in a zero-based array.


## Graphs

- Depth First Search (DFS)
  - recursive search
  - tree traversals (any order) are special forms of DFS
  - Must leave a visit mark to prevent infinit loop
  - bad when there exists a very deep tree

- Breadth First Search (BFS)
  - visit a nodes' all adjacents before move on to adjacents' adjacents
  - use a queue to keep track of nodes need to search next


## Bit Manipulation

- and (a & b)
- or (a | b)
- xor (a ^ b)
- negation (~a)
- left shift (a << n) - multiply by 2^n
- right shift (a >> n)

Example:

extract the i-th bit from n

```
n & (1 << i)
```

flip the i-th bit to 1

```
n | (1 << i)
```

flip the i-th bit to 0

```
n & (~(1 << i))
```

## Mathematics

### prime numbers and divisibility

- prime factorization
- greatest common divisor
- least common multiple

- primality test: iterate from 1 to sqrt(n). (think: why sqrt?)
- the Sieve of Eratosthenes (implement it!)

### Probability

P(A && B) = P(B|A)P(A)

P(A || B) = P(A) + P(B) - P(A && B)

Indipendence: P(A && B) = P(A)P(B)

Mutual Exclusivity: P(A && B) = 0, P(A || B) = P(A) + P(B)




