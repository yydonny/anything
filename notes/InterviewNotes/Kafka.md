
## Kafka Design

### The Broker

- high-throughput, high volume event streams
- large data backlogs
- low-latency delivery
- support partitioned, distributed, real-time processing feeds to create new, derived feeds
- fault-tolerance in the presence of machine failures.

Efficiency is a key to effective multi-tenant operations. By being very fast we help ensure that the **application will tip-over under load before the infrastructure**. This is particularly important when trying to run a centralized service that supports dozens or hundreds of applications on a centralized cluster.

All data is immediately written to a **persistent log on the filesystem** without necessarily flushing to disk. In effect this just means that it is transferred into the kernel's pagecache. A **persistent queue** could be built on simple reads and appends to files as is the case with logging solutions. This way all operations are O(1) and reads do not block writes or each other. 

The server appends chunks of messages to its log in one go, and the consumer fetches large linear chunks at a time. **Batching** leads to larger network packets, larger sequential disk operations, contiguous memory blocks, and so on, allows Kafka to group messages together and amortize the overhead of the network roundtrip. 

The message log maintained by the broker is itself just a directory of files, each populated by message sequences written to disk in the **same format** as the producer and consumer. Using the **zero-copy optimization**, data is copied into pagecache exactly once and reused on each consumption. This allows messages to be consumed at a rate that approaches the limit of the network connection.

Kafka supports **compressing** with an efficient batching format. A batch of messages can be clumped together compressed and sent to the server. This batch of messages will be written in compressed form and will remain compressed in the log and will only be decompressed by the consumer.


### The Producer



### The Consumer




## Advantage

- Extremely high performance, designed to handle web scale data stream between thousands of consumer/producers with very low latency.
- Durable message queue. This allows producers and consumers to work at a diverse pace, and makes maintaining large number of consumers of one queue easy.


## Topics

Kafka topics are partitioned to provide scalability. __All message in one partition is kept together in one node__. Producer is responsible of choosing partition for each message, this can be done randomly, using round robin or on a semantic basis. Producers push message directly to the broker that’s the leader of the partition. By allowing the producer to control data partitioning, localized processing is easily achievable.

Because Kafka consumers control the offset, it can achieve exact-once delivery by writing consumed offset along with the processed data in one single transaction.

So effectively Kafka supports exactly-once delivery in Kafka Streams, and the transactional producer/consumer can be used to provide exactly-once delivery when transfering and processing data between Kafka topics. Exactly-once delivery for other destination systems generally requires cooperation with such systems, but Kafka provides the offset which makes implementing this feasible (see also Kafka Connect). Otherwise, Kafka guarantees at-least-once delivery by default, and allows the user to implement at-most-once delivery by disabling retries on the producer and committing offsets in the consumer prior to processing a batch of messages.

The unit of replication is the topic partition. Each partition in Kafka has a single leader and zero or more followers. All reads and writes go to the leader of the partition. The logs on the followers are identical to the leader's log.

Followers consume messages from the leader just as a normal Kafka consumer would and apply them to their own log.

A message is considered committed when all in sync replicas for that partition have applied it to their log. Only committed messages are ever given out to the consumer. This means that the consumer need not worry about potentially seeing a message that could be lost if the leader fails.

Producers, on the other hand, have the option of either waiting for the message to be committed or not, depending on their preference for tradeoff between latency and durability.
The downside of majority vote is that it doesn't take many failures to leave you with no electable leaders. To tolerate one failure requires three copies of the data, and to tolerate two failures requires five copies of the data. In our experience having only enough redundancy to tolerate a single failure is not enough for a practical system, but doing every write five times, with 5x the disk space requirements and 1/5th the throughput, is not very practical for large volume data problems. This is likely why quorum algorithms more commonly appear for shared cluster configuration such as ZooKeeper but are less common for primary data storage. For example in HDFS the namenode's high-availability feature is built on a majority-vote-based journal, but this more expensive approach is not used for the data itself.


Sent from my iPhone







