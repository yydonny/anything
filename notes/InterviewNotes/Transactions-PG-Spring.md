

# PostgreSQL Transaction





# Spring Tansactions

Spring provides a convenient translation from technology-specific exceptions, such as `SQLException` to its own exception class hierarchy, which has `DataAccessException` as the root exception. These exceptions wrap the original exception so that there is never any risk that you might lose any information about what might have gone wrong.

In addition to JDBC exceptions, Spring can also wrap JPA- and Hibernate-specific exceptions, converting them to a set of focused runtime exceptions. This lets you handle most **non-recoverable persistence exceptions** in only the appropriate layers, without having annoying boilerplate catch-and-throw blocks and exception declarations in your DAOs. As mentioned above, JDBC exceptions are also converted to the same hierarchy.

- How to config a `DataSource` in Spring?
  - What support are there for development/testing?

Spring obtains a connection to the database through a `DataSource`. A `DataSource` is part of the JDBC specification and is a generalized connection factory. As a developer, you do not necessarily have to know how the production data source is configured. When you use Spring’s JDBC layer, you can obtain a data source from JNDI, or you can configure your own with a connection pool implementation provided by a third party.

The `DataSourceUtils` class is a convenient and powerful helper class that provides static methods to obtain connections from JNDI and close connections if necessary. It supports thread-bound connections with, for example, `DataSourceTransactionManager`.

- Template pattern and JDBC Template
  - Be familiar with the three `JdbcTemplate` callback interfaces that can be used with queries.

`JdbcTemplate` is the central class in the JDBC core package. It handles the creation and release of resources which helps you avoid common errors. It performs the basic tasks of the core JDBC workflow (such as statement creation and execution), leaving application code to provide SQL and extract results.

It runs SQL queries, Updates statements and stored procedure calls, Performs iteration over ResultSet instances and extraction of returned values.

It catches JDBC exceptions and translates them to the generic, more informative exception hierarchy.

When you use the `JdbcTemplate` for your code, you need only to implement callback interfaces. The `PreparedStatementCreator` callback interface creates a prepared statement, providing SQL and any necessary parameters. The same is true for the `CallableStatementCreator` interface, which creates callable statements. The `RowCallbackHandler` interface extracts values from each row of a ResultSet.

- When does the JDBC template acquire (and release) a connection - for every method called or once per template? Why?

Instances of the `JdbcTemplate` class are thread-safe, once configured.

- How does the `JdbcTemplate` support generic queries? How does it return objects and lists/maps of objects?


## Transaction management

Comprehensive transaction support is among the most compelling reasons to use the Spring Framework. The Spring Framework provides a consistent programming model across different transaction APIs, such as JTA, JDBC, Hibernate, and the JPA.

Traditionally, Java EE developers have had two choices for transaction management: global or local transactions, both of which have profound limitations.

**Global transactions** let you work with multiple transactional resources, typically relational databases and message queues. **The application server manages global transactions through the JTA**. Furthermore, a JTA UserTransaction normally needs to be sourced from JNDI, meaning that you also need to use JNDI in order to use JTA.

Previously, the preferred way to use global transactions was through **EJB CMT (Container Managed Transaction)**. CMT is a form of declarative transaction that removes the need for JNDI lookups, although the use of EJB itself necessitates the use of JNDI. The significant downside is that **CMT is tied to JTA and an application server environment**. Also, it is **only available if one chooses to implement business logic in EJBs** (or at least behind a transactional EJB facade).

**Local transactions** are resource-specific, such as a transaction associated with a JDBC connection. They cannot work across multiple transactional resources. For example, **code that manages transactions by using a JDBC connection cannot run within a global JTA transaction**. Because the application server is not involved in transaction management, it cannot help ensure correctness across multiple resources. Another downside is that local transactions are invasive to the programming model.

Spring lets application developers use a consistent programming model in any environment. You write your code once, and it can benefit from **different transaction management strategies in different environments**. The Spring Framework provides both declarative and programmatic transaction management.

With programmatic transaction management, developers work with the Spring Framework transaction abstraction, which can run over any underlying transaction infrastructure. With the preferred declarative model, developers typically write little or no code related to transaction management and, hence, do not depend on the Spring Framework transaction API or any other transaction API.

Typically, you need an application server’s JTA capability only if your application needs to handle transactions across multiple resources, which is not a requirement for many applications. Many high-end applications use a single, highly scalable database instead. Stand-alone transaction managers (such as Atomikos and JOTM) are other options.


The key to the Spring transaction abstraction is the notion of a **transaction strategy**. A transaction strategy is defined by the `PlatformTransactionManager` interface. Because `PlatformTransactionManager` is an interface, it can be easily mocked or stubbed as necessary. It is not tied to a lookup strategy, such as JNDI. The implementations are defined like any other bean in the IoC container.

The `getTransaction` method returns a TransactionStatus object, depending on a `TransactionDefinition` parameter - Propagation, Isolation, Read-Only, Timeout. It might represent a new transaction or can represent an existing transaction, if **a matching transaction exists in the current call stack**. The implication in this latter case is that, as with Java EE transaction contexts, **a TransactionStatus is associated with a thread of execution**.

**A TransactionStatus is associated with a thread of execution**.

`PlatformTransactionManager` implementations normally require knowledge of the environment in which they work: JDBC, JTA, Hibernate, and so on. If you use JTA in a Java EE container, then you use a container `DataSource`, obtained through JNDI, in conjunction with Spring’s `JtaTransactionManager`. The `JtaTransactionManager` does not need to know about the DataSource or any other specific resources because it uses the **container’s global transaction management infrastructure**.

The preferred approach is to use Spring’s highest-level template based persistence integration APIs or to use native ORM APIs with *transaction-aware factory beans or proxies*. These **transaction-aware solutions** internally handle resource creation and reuse, cleanup, optional transaction synchronization, and exception mapping. 

For example, if you use the Spring `JdbcTemplate` or `jdbc.object` package, **correct connection retrieval occurs behind the scenes** and you need not write any special code. If an existing transaction already has a connection synchronized (linked) to it, that connection is reused. Otherwise, the method call triggers the creation of a new connection, which is (optionally) synchronized to any existing transaction and made available for subsequent reuse in that same transaction.

At the very lowest level exists the `TransactionAwareDataSourceProxy` class. This is a proxy for a target `DataSource`, which wraps the target `DataSource` to add awareness of Spring-managed transactions. In this respect, it is similar to a transactional JNDI `DataSource`, as provided by a Java EE server.

The Spring Framework’s declarative transaction management is similar to EJB CMT, in that you can specify transaction behavior (or lack of it) down to the individual method level.

EJB CMT default behavior automatically rolls back the transaction only on a system exception (usually a runtime exception). While the Spring follows EJB convention (**roll back is automatic only on unchecked exceptions**), it is often useful to customize this behavior.

Spring’s transaction abstraction is generally application server-agnostic. Additionally, Spring’s `JtaTransactionManager` class (which can optionally perform a JNDI lookup for the JTA `UserTransaction` and `TransactionManager` objects) autodetects the location for the latter object, which varies by application server.

Spring’s `JtaTransactionManager` is the standard choice to run on Java EE application servers and is known to work on all common servers. Advanced functionality, such as transaction suspension, works on many servers as well. However, for fully supported transaction suspension and further advanced integration, Spring includes special adapters for WebLogic Server and WebSphere.


# Distributed transactions and atomicity

A distributed transaction is one that involves more than one **transactional resource**. Examples of transactional resources are the connectors for communicating with relational databases and messaging middleware.


In a typical example, a JMS message triggers a database update. Broken down into a timeline, a successful interaction goes something like this:

1.Start messaging transaction
2.Receive message
3.Start database transaction
4.Update database
5.Commit database transaction
6.Commit messaging transaction

If you need close-to-bulletproof guarantees that your application's transactions will recover after an outage, including a server crash, then **Full XA with 2PC** is your only choice. The shared resource that is used to synchronize the transaction in this case is a special transaction manager. In Java, the protocol is exposed through a JTA `UserTransaction`.

- **Full XA with 2PC**
- **XA with 1PC Optimization**- avoid the overhead of 2PC if the transaction includes a single resource
- **XA and the Last Resource Gambit** - when all but one resource is XA-capable, order the resources and use the non-XA resource as a casting vote.

- **Shared Transaction Resource pattern**

An use of this pattern is the case of message-driven update of a single database. Messaging-middleware need to store their data somewhere, often in a relational database. To implement this pattern, all that's needed is to **point the messaging system at the same database the business data is going into**. This pattern relies on the messaging-middleware vendor exposing the details of its storage strategy.

Enterprise-level database vendors all support the notion of synonyms (or the equivalent), where tables in one schema are declared as synonyms in another. In that way data that is partitioned physically in the platform can be addressed transactionally from the same Connection in a JDBC client. For example, the implementation of the shared-resource pattern with ActiveMQ in a real system (as opposed to the sample) would usually involve creating synonyms for the messaging and business data.

- **Best Efforts 1PC pattern**

This is a non-XA pattern that involves a **synchronized single-phase commit of a number of resources**. Because the 2PC is not used, it can never be as safe as an XA transaction, but is often good enough if the participants are aware of the compromises. Many high-volume, high-throughput transaction-processing systems are set up this way to improve performance.

The basic idea is to **delay the commit of all resources as late as possible** in a transaction so that the **only thing that can go wrong is an infrastructure failure**.

The important point is that the commit or rollback happens in the reverse order of the business ordering in the resources. 






































