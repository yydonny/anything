
## RDS


### General

- Monitor your memory, CPU, and storage usage with Amazon CloudWatch
- Scale up when you are approaching storage capacity limits.
- Enable automatic backups and set the backup window to occur during the daily low in write IOPS.
- If your application is caching the DNS of your DB instances, set a TTL of less than 30 seconds.
- Test failover to understand how long the process takes and to ensure that the application can automatically connect to the new DB instance.

### Tuning

*Database connections* – Consider constraining connections if you see high numbers of user connections in conjunction with decreases in performance and response time.

You can determine the number of database connections by associating your DB instance with a **parameter group** where the User Connections parameter is set to other than 0 (unlimited).

IOPS – For best IOPS performance, make sure your *typical working set will fit into memory* to minimize read and write operations.


### Read Replica

Updates made to the source DB instance are **asynchronously** copied to the Read Replica. You can reduce the load on your source DB instance by routing read to the Read Replica. Using Read Replicas, you can elastically scale out beyond the capacity constraints of a single DB instance *for read-heavy database workloads*. 

Read Replicas are supported by the MariaDB, MySQL, and PostgreSQL engines.

When you create a Read Replica, you first specify an existing DB instance as the source. Then Amazon RDS takes a snapshot and creates a read-only instance from the snapshot. RDS then uses the asynchronous replication method for the DB engine to update the Read Replica whenever there is a change to the source DB instance. The Read Replica operates as a DB instance that allows read-only connections. 

If Read Replica is in a different AWS Region, RDS sets up a secure communications channel between the source DB instance and the Read Replica. RDS establishes any AWS security configurations needed to enable the secure channel, such as adding security group entries.

MySQL read Replicas can be made writable, while PostgreSQL's can't.

Use cases:

- Scaling beyond a single DB instance for read-heavy database workloads.
- Serving read traffic while the source DB instance is unavailable (e.g. in backup or maintenance).
- Business reporting or data warehousing scenarios
- Implementing disaster recovery


You can *promote a Read Replica into a standalone DB instance*. When you promote a Read Replica, the DB instance is rebooted before it becomes available. This is useful in Implementing failure recovery.


When you promote a Read Replica, the process can take several minutes or longer to complete, depending on the size of the Read Replica. Once done, it's just like any other DB instance. For example, you can convert the new DB instance into a Multi-AZ DB instance, create Read Replicas from it, and perform point-in-time restore operations.

A Read Replica cannot be promoted to a standalone instance when it is in the backing-up status.


### Cross-Region Replication

- A source DB instance can have cross-region Read Replicas in multiple regions.
- You can only create a cross-region Amazon RDS Read Replica from a source Amazon RDS DB instance that is not a Read Replica of another Amazon RDS DB instance.
- Within an AWS Region, all cross-region Read Replicas created from the same source DB instance must *either* be in the same Amazon VPC *or* be outside of a VPC.
- Due to the limit on the number of access control list (ACL) entries for a VPC, we can't guarantee more than five cross-region Read Replica instances.













