
# AWS Test Problems

- progress: 41

## EC2

- Each instance must be associated with at least one security group.
- There's a limit in how many instances you can create in one region, depending on the instance type.
- EBS volumes are exposed as storage volumes that *persist independently* from the life of the instance it attaches to.

## EC2 ASG (Auto Scaling Groups)

A launch configuration is an instance configuration template that an Auto Scaling group uses to launch EC2 instances. 

When you create a launch configuration, you specify information for the instances. If you've launched an EC2 instance before, you specified the same information in order to launch the instance. 

## S3

- Max object size for S3 is 5TB (compared to 40TB in Glacier)

- S3 Cross-Region Replication
  - automatic, asynchronous
  - minimize latency in accessing objects
  - requires versioning in both buckets 
  - existing objects before CRR is set up are not automatically replicated 

- REDUCED_REDUNDANCY has an average annual expected loss of 0.01% of objects. Normal rate of S3 is 0.0000000001%
- Bucket policy allows granting access to other AWS accounts.
- Certain operations like delete can be guarded by MFA.

## CloudWatch

- CloudWatch keeps log data indefinitely by default.
- Although you can control access with IAM, restriction to specific resources metrics is not supported.

## IAM Role

IAM role is similar to an IAM user, but it is not associated with a specific person. An IAM role enables you to obtain temporary access keys that can be used to access AWS services and resources. 

- Federated user: Instead of creating an IAM user, you can use preexisting identities from AWS Directory Service, your LDAP, or a web identity provider (IdP). These are known as federated users.
- Cross-account access: You can grant another AWS account permissions to access your account’s resources.
- AWS service access: You can grant an AWS service the permissions needed to access your account’s resources.
- Applications on EC2: Instead of storing access keys within the EC2 instance, you can use an IAM role to manage temporary credentials for applications. You can create an **instance profile** that is attached to the instance. An instance profile contains the role and enables programs running on the EC2 instance to get temporary credentials.



## ELB

Classic load balancer can automatically expand the capacity of an Auto Scaling Group.

There're limits for ELB capacities:

- Load balancers per region: 20 *
- Target groups per region: 3000
- Targets per load balancer: 1000
- Listeners per load balancer: 50
- Security groups per load balancer: 5

Each target group can only be used for one load balancer.

## RDS

- data is copied to standby and read replica asynchronously.
























