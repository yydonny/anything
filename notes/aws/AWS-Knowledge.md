

## EC2 Placement Group

You can launch or start instances in a placement group, which determines how instances are **placed on underlying hardware**.

- **Cluster**: clusters instances into a low-latency group in a *single Availability Zone*
- **Spread**: spreads instances across underlying hardware
- **Partition**: spreads instances across logical partitions, ensuring that *instances in one partition do not share underlying hardware with instances in other partitions*

A instance can only belong to one placement group. Stopped instance can be added to other PG after creation.

For instances that are enabled for *enhanced networking*, traffic between instances within the same Region that is addressed using IPv4 or IPv6 addresses can use up to 5 Gbps for single-flow traffic and up to 25 Gbps for multi-flow traffic. A flow represents a single, point-to-point network connection.

Not all instance types support PG. You can also launch multiple instance types into a cluster placement group. However, this reduces the likelihood that the required capacity will be available for your launch to succeed. 


## S3 Server-Side Encryption (SSE)

Amazon S3 can encrypt your data at the object level as it writes it to disks in its data centers and decrypts it for you when you access it.

- SSE with Amazon S3-Managed Keys (**SSE-S3**): Each object is encrypted with a unique key employing strong multi-factor encryption. As an additional safeguard, it encrypts the key itself with a master key that it regularly rotates.
- SSE with AWS KMS-Managed Keys (**SSE-KMS**): There are separate permissions for the use of an envelope key (that is, a key that protects your encryption key) that provides added protection against unauthorized access. SSE-KMS also provides you with an audit trail of when your key was used and by whom.
- SSE with Customer-Provided Keys (**SSE-C**)

## ELB Basic

A load balancer accepts incoming traffic and routes requests to its *registered targets in one or more AZs*. To route traffic to multiple AZs, **Cross-zone load balancing** must be enabled and each AZ has to be enabled for ELB.

Load balancer accepts incoming traffic by specifying one or more **listeners**. Listeners connect between clients to underlying services.

- With **Application** Load Balancers and **Network** Load Balancers, you register targets in **target groups**, and route traffic to the target groups. 
- With **Classic** Load Balancers, you register *EC2 instances* with the load balancer. 

Load balancer is *most effective if you ensure that each enabled AZ has at least one registered target*. We recommend that you enable multiple AZs.

With Application Load Balancers, cross-zone load balancing is always enabled.

Load balancer is either **internal** or **Internet-facing**. Classic Load Balancer must be an Internet-facing. 

- Internet-facing load balancer have public IP addresses and can route requests from the Internet.
- Internal load balancer have only private IP addresses, can only route requests from clients with access to the VPC where the load balancer resides.

Both Internet-facing and internal load balancers **route requests to your targets using private IP addresses**.

An ELB works like an EC2 instance except for the OS, disk and CPU. ELB can route traffic to instance, IP (CIDR) or Lambda functions. Routing to publicly routable IP addresses is not supported. In the case of Lambda, the function is invoked by ELB.

The load balancer monitors the health of its registered targets and ensures that it routes traffic only to healthy targets. for Classic ELB, it can ping certain path or make connection attempt to check health of instances.

**Network Load Balancer** functions at the 4th layer of the OSI model. It can handle millions of requests per second. By default, each load balancer node distributes traffic across the registered targets in its Availability Zone only, unless you enable multi AZ.

IAM and Lambda
=============

IAM role – An IAM role is similar to an IAM user, but not associated with a speciﬁc person. An IAM role enables you to obtain **temporary access keys** that can be used to access AWS services and resources.

A resource is owned by the AWS account of the **principal entity** (root account, an IAM user, or an IAM role) that created the resource.

Example:

```json
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "suibian",
      "Effect": "Allow",
      "Action": [
        "lambda:CreateFunction"
      ],
      "Resource": "*"
    },
    {
      "Sid": "...",
      "Effect": "Allow",
      "Action": [
        "iam:PassRole"
      ],
      "Resource": "arn:aws:iam::account-id:role/*"
    }
  ]
}
```


The policy is an identity-based policy so you don't need to specify the principal. When you attach policy to a user, the user is the implicit principal.

The first statement grants permissions `lambda:CreateFunction` on all resources. The second statement grants permissions for the IAM action (iam:PassRole) on IAM roles.

In AWS Lambda, the primary resources are a **Lambda function** and an **event source mapping**. You create an event source mapping in **Lambda pull model** to **associate a Lambda function with an event source**.

AWS Lambda supports both identity-based (IAM policies) and resource-based policies.

Lambda function can have resource-based policies associated with it, and these policies are referred to as **Lambda function policies**. You can use it to grant cross-account permissions. For example, you can grant S3 permissions to invoke one of your Lambda function without creating an IAM role. 

