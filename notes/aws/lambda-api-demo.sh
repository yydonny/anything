
region='us-east-2'
account_id='970558770930'

alias apigateway="aws --profile dev1 --region $region apigateway"
alias lambda="aws --profile dev1 --region $region lambda"

# ===========================================================================
#                              test
# ===========================================================================

api_id=$(apigateway get-rest-apis | jq -r '.items[0].id')

echo '{
    "operation": "echo",
    "payload": {
        "Item": {
            "id": "1234ABCD",
            "number": 5
        }
    }
}' > event.json

api_url="https://$api_id.execute-api.$region.amazonaws.com/prod/api001"
http post $api_url < event.json

lambda invoke --function-name api001 \
  --payload 'fileb://event.json' output.json


# ===========================================================================
# ELB Setup
# ===========================================================================

fn_arn=$(lambda list-functions | jq -r '.Functions[0].FunctionArn')
target_group_arn='arn:aws:elasticloadbalancing:us-east-2:970558770930:targetgroup/my-functions/acc4881d3545f471'

lambda add-permission --function-name $fn_arn \
  --statement-id elb1 \
  --principal 'elasticloadbalancing.amazonaws.com' \
  --action 'lambda:InvokeFunction' \
  --source-arn $target_group_arn



# ===========================================================================
# Deploy Lambda function package
# ===========================================================================

# Must assign a role for execution
pkg_path='fn2.zip'
handler='elbfn1.main'
lambda create-function --function-name elbfn1 \
  --zip-file fileb://$pkg_path \
  --role 'arn:aws:iam::970558770930:role/lambda-role-1' \
  --handler $handler \
  --runtime python3.6 \
  --timeout 15 \
  --memory-size 512

# test the function directly
aws lambda invoke --profile dev1 --region us-east-2 \
  --function-name api001 --payload 'fileb://test-payload.json' outputfile.txt

# ===========================================================================
#                  Integrate with API Gateway
# ===========================================================================

aws --profile dev1 --region us-east-2 \
  apigateway create-rest-api --name api001

api_id='b4csbatlvc'

# retrieve resource id of the root path '/'
aws --profile dev1 --region us-east-2 \
  apigateway get-resources --rest-api-id $api_id |
  jq '.items[-1].id'

api_path_id='ypcjzsr7vj'

# create resource path '/api001'
aws --profile dev1 --region us-east-2 \
  apigateway create-resource --rest-api-id $api_id \
  --path-part api001 \
  --parent-id $api_path_id

api_api001_id='aglpfn'

# declare an POST api at /api001
aws --profile dev1 --region us-east-2 \
  apigateway put-method --rest-api-id $api_id --resource-id $api_api001_id \
  --http-method POST --authorization-type NONE

# Connect API Gateway to the Lambda function
api001_endpoint_arn="arn:aws:apigateway:${region}:lambda:path/2015-03-31/functions/arn:aws:lambda:${region}:${account_id}:function:api001/invocations"
aws --profile dev1 --region us-east-2 \
  apigateway put-integration --rest-api-id $api_id --resource-id $api_api001_id \
  --http-method POST --type AWS --integration-http-method POST \
  --uri $api001_endpoint_arn

# declare response type for the gateway API
aws --profile dev1 --region us-east-2 \
  apigateway put-method-response --rest-api-id $api_id --resource-id $api_api001_id \
  --http-method POST --status-code 200 --response-models '{"application/json":"Empty"}'

# declare response type for the Lambda endpoint
aws --profile dev1 --region us-east-2 \
  apigateway put-integration-response --rest-api-id $api_id --resource-id $api_api001_id \
  --http-method POST --status-code 200 --response-templates '{"application/json":""}'

# finalize the deployment
aws --profile dev1 --region us-east-2 \
  apigateway create-deployment --rest-api-id $api_id --stage-name prod
api_deploy_id='sd6441'


# TO be continued...
# https://docs.aws.amazon.com/lambda/latest/dg/with-on-demand-https-example.html#with-on-demand-https-add-permission

# Grant permission to API Gateway to invoke the function
aws --profile dev1 --region us-east-2 \
  lambda add-permission --function-name api001 \
  --statement-id 'apigateway-test-1' --action lambda:InvokeFunction \
  --principal 'apigateway.amazonaws.com' \
  --source-arn "arn:aws:execute-api:$region:$account_id:$api_id/*/POST/api001"

aws --profile dev1 --region us-east-2 \
  lambda add-permission --function-name api001 \
  --statement-id 'apigateway-prod-1' --action lambda:InvokeFunction \
  --principal 'apigateway.amazonaws.com' \
  --source-arn "arn:aws:execute-api:$region:$account_id:$api_id/prod/POST/api001"

aws --profile dev1 --region us-east-2 \
  lambda remove-permission --function-name api001 --statement-id 'apigateway-test-1'
aws --profile dev1 --region us-east-2 \
  lambda remove-permission --function-name api001 --statement-id 'apigateway-prod-1'

