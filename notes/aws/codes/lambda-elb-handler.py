import json


def respond(body):
    response = {
        "isBase64Encoded": False,
        "statusCode": 200,
        "statusDescription": "200 OK",
        "headers": {"Content-Type": "application/json"},
        "body": None,
    }
    response["body"] = body
    return response


def handle(source, event):
    if "ELB-HealthChecker" in source:
        return respond("I'm fine")
    else:
        return respond(json.dumps(event))


def main(event, context):
    source = event["headers"]["user-agent"]
    return handle(source, event)

