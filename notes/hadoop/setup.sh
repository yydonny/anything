
# ---------------------------------------------------------------------------

cat > /etc/hosts << __dddd
10.0.1.205 ip-10-0-1-205
10.0.1.206 ip-10-0-1-206
10.0.1.207 ip-10-0-1-207
10.0.1.208 ip-10-0-1-208
__dddd

WORDS="ip-10-0-1-206 ip-10-0-1-207 ip-10-0-1-208"
hostname;jps
for i in $WORDS; do
  ssh $i "hostname;jps"
done

for i in $WORDS; do
  scp ~/hadoop/etc/hadoop/hadoop-env.sh ubuntu@$i:hadoop/etc/hadoop/hadoop-env.sh
done

# ---------------------------------------------------------------------------
# Environments

export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64
export HADOOP_PREFIX="$HOME/hadoop-2.7.4"

export HADOOP_HOME=$HADOOP_PREFIX
export HADOOP_COMMON_HOME=$HADOOP_PREFIX
export HADOOP_CONF_DIR=$HADOOP_PREFIX/etc/hadoop
export HADOOP_HDFS_HOME=$HADOOP_PREFIX
export HADOOP_MAPRED_HOME=$HADOOP_PREFIX
export HADOOP_YARN_HOME=$HADOOP_PREFIX

export PATH=$PATH:$HADOOP_PREFIX/bin:$HADOOP_PREFIX/sbin


# ---------------------------------------------------------------------------
# Using S3 in place of HDFS
# https://hadoop.apache.org/docs/current/hadoop-aws/tools/hadoop-aws/index.html

cd $HADOOP_PREFIX/share/hadoop/common/lib
find ../../tools/lib/ -name '*aws*' -exec ln -s {} \;
ln -s ../../tools/lib/jackson-databind-2.2.3.jar
ln -s ../../tools/lib/jackson-core-2.2.3.jar
ln -s ../../tools/lib/jackson-annotations-2.2.3.jar

cd
hadoop fs -mkdir -p /test
hadoop fs -ls /


# when this error happens
# 17/10/29 21:42:32 ERROR Error Launching job : The ownership on the staging directory /tmp/hadoop-yarn/staging/ec2-user/.staging is not as expected. It is owned by . The directory must be owned by the submitter ec2-user or by ec2-user
aws s3 rm s3://ettouy-hadoop-us-east-1/tmp/hadoop-yarn --recursive


# ------------------------------------------
# Start single node

hdfs namenode -format

hadoop-daemon.sh start namenode
hadoop-daemon.sh start datanode

yarn-daemon.sh start resourcemanager
yarn-daemon.sh start nodemanager

# ------------------------------------------
# start cluster

# If on EC2, make sure node can acccess each other through ANY PORT (not just ssh)!!

# master
hadoop-daemon.sh start namenode

# slaves
hadoop-daemon.sh start datanode

# master
yarn-daemon.sh start resourcemanager

# slaves
yarn-daemon.sh start nodemanager

yarn node -list
# 17/10/29 20:58:53 INFO client.RMProxy: Connecting to ResourceManager at ip-172-31-32-153/172.31.32.153:8032
# Total Nodes:2
#          Node-Id             Node-State Node-Http-Address       Number-of-Running-Containers
# ip-172-31-32-153.ec2.internal:41301             RUNNING ip-172-31-32-153.ec2.internal:8042                                 0

yarn application -list

yarn applicationattempt -list
yarn container -list

# 1. hadoop install path must be the same as $HADOOP_PREFIX set in the master
# 2. edit hadoop-env.sh file and set JAVA_HOME for Hadoop
# 3. ensure ssh direct sign in

# define slave nodes (this file is used by both HDFS and YARN master)
nano $HADOOP_PREFIX/etc/hadoop/slaves

start-dfs.sh
start-yarn.sh

# ------------------------------------------

yarn-daemon.sh stop resourcemanager
yarn-daemon.sh stop nodemanager

hadoop-daemon.sh stop datanode
hadoop-daemon.sh stop namenode
hadoop-daemon.sh stop secondarynamenode

hdfs dfsadmin -report

# Run Distributed shell with 2 containers and executing the script `date`.

hadoop jar $HADOOP_PREFIX/share/hadoop/yarn/hadoop-yarn-applications-distributedshell-*.jar \
  org.apache.hadoop.yarn.applications.distributedshell.Client \
  --jar $HADOOP_PREFIX/share/hadoop/yarn/hadoop-yarn-applications-distributedshell-*.jar \
  --shell_command date --num_containers 4 --master_memory 2048

hadoop jar $HADOOP_PREFIX/share/hadoop/mapreduce/hadoop-mapreduce-examples-*.jar randomwriter out2

hadoop fs -ls out2/

hadoop fs -get out2/part-m-00000

