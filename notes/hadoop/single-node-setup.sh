
# ---------------------------------------------------------------------------
# Prepare config files

python3 -m pip install pyyaml lxml
wget https://raw.githubusercontent.com/yangyd/hadoop-yaml-config/master/hadoop-yaml-config.py
chmod +x hadoop-yaml-config.py

./hadoop-yaml-config.py -d confs single-node-config.yml

cp confs/single-node/* ~/hadoop-2.7.4/etc/hadoop

# ---------------------------------------------------------------------------
# Environments

export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64
export HADOOP_PREFIX="$HOME/hadoop-2.7.4"

export HADOOP_HOME=$HADOOP_PREFIX
export HADOOP_COMMON_HOME=$HADOOP_PREFIX
export HADOOP_CONF_DIR=$HADOOP_PREFIX/etc/hadoop
export HADOOP_HDFS_HOME=$HADOOP_PREFIX
export HADOOP_MAPRED_HOME=$HADOOP_PREFIX
export HADOOP_YARN_HOME=$HADOOP_PREFIX

export PATH=$PATH:$HADOOP_PREFIX/bin:$HADOOP_PREFIX/sbin

# ------------------------------------------
# Start single node

hdfs namenode -format

# namenode
hadoop-daemon.sh start namenode

# datanode
hadoop-daemon.sh start datanode

# yarn
yarn-daemon.sh start resourcemanager
yarn-daemon.sh start nodemanager

# Startup result:
#      ubuntu@ip-172-31-18-235:~$ jps
#      2992 Jps
#      2499 DataNode
#      2598 ResourceManager
#      2410 NameNode
#      2843 NodeManager

hdfs dfsadmin -report

# Run Distributed shell with 2 containers and executing the script `date`.
hadoop jar $HADOOP_PREFIX/share/hadoop/yarn/hadoop-yarn-applications-distributedshell-*.jar \
  org.apache.hadoop.yarn.applications.distributedshell.Client \
  --jar $HADOOP_PREFIX/share/hadoop/yarn/hadoop-yarn-applications-distributedshell-*.jar \
  --shell_command date --num_containers 4 --master_memory 2048

yarn-daemon.sh stop resourcemanager
yarn-daemon.sh stop nodemanager

hadoop-daemon.sh stop datanode
hadoop-daemon.sh stop namenode




