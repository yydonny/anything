
# Consul

## Agent

The Consul **agent** is the core process of Consul. The agent maintains membership information, registers services, runs checks, responds to queries, and more. The agent must run on every node that is part of a Consul cluster.

Any agent may run in one of two modes: **client** or **server**. A *server node* takes on the additional responsibility of *being part of the consensus quorum*. These nodes take part in **Raft** and provide strong consistency and availability in the case of failure. Server agents usually should be run on dedicated instances -- they are more resource intensive than a client node. 

*Client nodes make up the majority of the cluster, and they are very lightweight* as they interface with the server nodes for most operations and maintain very little state of their own. Each data center must have at least one server, though a cluster of 3 or 5 servers is recommended. 

All other agents run in client mode. A client is a very lightweight process that registers services, runs health checks, and forwards queries to servers. The *agent must be running on every node that is part of the cluster*.

When an agent is first started, it does not know about any other node in the cluster. To discover its peers, it must join the cluster. Once a node joins, this information is gossiped to the entire cluster, meaning *all nodes will eventually be aware of each other*. If the agent is a *server*, existing servers will begin *replicating to the new node*.

In the case of a network failure, some nodes may be unreachable by other nodes. In this case, unreachable nodes are marked as **failed**. It is impossible to distinguish between a network failure and an agent crash, so both cases are handled the same. Once a node is marked as failed, this information is updated in the service catalog.

When a node leaves, it specifies its intent to do so, and the cluster marks that node as having **left**. Unlike the failed case, *all of the services provided by a node are immediately deregistered*. If the agent was a server, replication to it will stop.

To prevent an accumulation of dead nodes (nodes in either failed or left states), Consul will automatically remove dead nodes out of the catalog. This process is called reaping.



## DNS

Consul serves as a DNS for registered services. The DNS name for services is `NAME.service.consul`. By default, all DNS names are always in the consul namespace. The service subdomain tells Consul we're querying services, and the NAME is the name of the service.

For example, lookups like `redis.service.us-east-1.consul` automatically translates to a lookup of nodes that provide the redis service, are located in the us-east-1 datacenter, and have no failing health checks.

By default, Consul will listen on `127.0.0.1:8600` for DNS queries in the consul. domain, without support for further DNS recursion.

There are a few ways to use the DNS interface. One option is to use a custom DNS resolver library and point it at Consul. Another option is to set Consul as the DNS server for a node and provide a recursors configuration so that non-Consul queries can also be resolved. The last method is to forward all queries for the "consul." domain to a Consul agent from the existing DNS server.


## Cluster

Start an agent in server mode:

```bash
# Each node in a cluster must have a unique name
node_name=agent-one
# must be accessible by all other nodes in the cluster.
listen=172.20.20.10
# the number of additional server nodes we are expecting to join. 
expect_other=1

consul agent -server -data-dir=/tmp/consul -config-dir=/etc/consul.d \
  -enable-script-checks=true \
  -bootstrap-expect=$expect_other \
  -node=$node_name \
  -bind=$listen
```

And then ask it to join the other server agent:

```bash
consul join 172.20.20.11
```

To join a cluster, a Consul agent *only needs to learn about one existing member*. After joining the cluster, the agents gossip with each other to propagate full membership information.

Consul facilitates *auto-join in public cloud* by enabling the auto-discovery of instances in AWS, Google Cloud or Azure with a given resource tag key/value.



## KV Store



## Consul Connect

Connect is a feature that create dynamic proxies for registered service. It accepts inbound connections on a dynamically allocated port, verifies and authorizes the TLS connection, and proxies back a standard TCP connection to the process.

Realistically, You can declare one service as dependency of another, then the service *uses a local port listened by Consul agent* to talk to its dependency rather than directly attempting to connect. 

Also you can use intentions to define which services may communicate.

Consul loads all configuration files in the configuration directory. Intentions allow services to be segmented via a centralized control plane (Consul).

```
consul agent -dev -config-dir=/local/etc/consul.d
```




# Vault

## Key-value secrets

Secrets written to Vault are encrypted and then written to backend storage. The backend storage mechanism never sees the unencrypted value and doesn't have the means necessary to decrypt it without Vault.

`vault kv` writes key-value pair to a specific *secret path*. The path prefix tells Vault which **secrets engine** to use for the request. This is similar to mounting a device to a POSIX path. By default, Vault enables the *kv secret engine* at the path `secret/`. `kv` is used to store arbitrary secrets within the configured physical storage for Vault.

Secret engine indicates the purpose for which you use Vault to manage secret (credentials), which dictates how exactly secrets are generated and used. Example of secret engines are public clouds, databases, SSH login, and MQs.

When a secrets engine is no longer needed, it can be disabled. When a secrets engine is disabled, all secrets are deleted and the corresponding Vault data and configuration is removed.

## Dynamic secrets

Dynamic secrets are *generated when they are accessed*. Dynamic secrets do not exist until they are read, and Vault has built-in revocation mechanisms, dynamic secrets can be revoked immediately after use, minimizing the amount of time the secret existed.

Example of using *AWS secret engine* to generate dynamic secret. Note that in the last step the access_key is generated with AWS on demand:

```bash
# Config vault with AWS credentials
vault write aws/config/root \
    access_key=AKIAI4SGLQPBX6CSENIQ \
    secret_key=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

# Config a role with which some IAM privilege are associated
vault write aws/roles/my-role policy=-<<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "Stmt1426528957000",
      "Effect": "Allow",
      "Action": [
        "ec2:*"
      ],
      "Resource": [
        "*"
      ]
    }
  ]
}
EOF

# Generate a new access_key for this role
vault read aws/creds/my-role

```

A generated key has a **lease_id** and **lease_duration**. Once the secret is revoked, the access keys are no longer valid.

```
$ vault read aws/creds/my-role
Key                Value
---                -----
lease_id           aws/creds/my-role/0bce0782-32aa-25ec-f61d-c026ff22106e
lease_duration     768h
lease_renewable    true
access_key         AKIAJELUDIANQGRXCTZQ
secret_key         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
security_token     <nil>
```

## Authentication

Vault supports many different authentication mechanisms, but they all funnel into a single "session token", which we call the **Vault token**.

The **root token** is the initial access token to configure Vault. It has root privileges and can perform any operation within Vault.

You can create more tokens with `vault token create` 
By default, this will create a child token of your current token that inherits all the same policies. When the parent token is revoked, children can also be revoked cascadingly.

Vault tokens are more like session IDs in a web app. In practice, operators should not use the `token create` to generate Vault tokens for users or machines. Instead, they should authenticate to Vault using one of the configured auth methods such as GitHub, LDAP, AppRole, etc.

Example authentication with Github:

```bash
# Auth path is similar to secret path
vault auth enable -path=github github

# configuration for Github login
vault write auth/github/config organization=hashicorp
vault write auth/github/map/teams/my-team value=default,my-policy

# do login
vault login -method=github
```

After login, a Vault token will be granted. The Vault client app will cache the token locally.

## Access Control

Access policies are authored in special format called HCL which it is JSON compatible.

## Deployment

Vault server is configured with HCL too.

```conf
storage "consul" {
  address = "127.0.0.1:8500"
  path    = "vault/"
}

listener "tcp" {
 address     = "127.0.0.1:8200"
 tls_disable = 1
}
```

**Initialization** is necessary when the server is started against a new backend that has never been used with Vault before. When running in HA mode, this happens once per cluster, not per server.

During initialization, the encryption keys are generated, unseal keys are created, and the initial root token is setup. To initialize Vault use `vault operator init`. This is an unauthenticated request, but it only works on brand new Vaults with no data.


Initialization outputs two incredibly important pieces of information: the **unseal keys** and the **initial root token**. This is the only time ever that all of this data is known together by Vault.

In a real deployment scenario, you would never save these keys together. Instead, you would likely use Vault's PGP and Keybase.io support to encrypt each of these keys with the users' PGP keys. This prevents one single person from having all the unseal keys.


## Seal/Unseal 

Every initialized Vault server starts in the **sealed state**. From the configuration, Vault can access the physical storage, but it *can't read any of it* because it doesn't know how to decrypt it. The process of teaching Vault how to decrypt the data is known as **unsealing** the Vault.

*Unsealing has to happen every time Vault starts*. It can be done via the API and via the command line. To unseal the Vault, you must have the **threshold number of unseal keys**. The unseal process is stateful, the Vault can be unsealed from multiple computers and the keys should never be together. A single malicious operator does not have enough keys to be malicious.

Finally, you have to authenticate with the *initial root token* in order to perform root operation.

A single operator is allowed *reseal* the vault. This lets a single operator lock down the Vault in an emergency without consulting others. When the Vault is sealed again, it clears all of its state (including the encryption key) from memory.































