import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class WordDistance {  
    
    private Map<String, List<Integer>> position = new HashMap<>();
    
/**
   * This method will be called with the paragraph to be analyzed
   * <p>
   * Implement this method to support your implementation of the closest word method below
   *
   * @param paragraph A string containing the input paragraph
   */
  public void input(String paragraph) {
      String[] words = tokenize(paragraph);
      for (int i = 0; i < words.length; i++) {
          if (!position.containsKey(words[i])) {
              position.put(words[i], new ArrayList<>());
          }
          position.get(words[i]).add(i);
      }
      
  }
  
  private String[] tokenize(String s) {
      String normalized = s.toLowerCase().replaceAll("\\.", "");
      return normalized.split("\\s+");
  }
  
  private List<Integer> merge(List<Integer> list1, List<Integer> list2) {
      List<Integer> result = new LinkedList<Integer>();
      int i = 0;
      int j = 0;
      while (true) {
          if (i >= list1.size() || j >= list2.size()) {
              break;
          }
          if (list1.get(i) < list2.get(j)) {
              result.add(list1.get(i));
              i += 1;
          } else {
              result.add(list2.get(j));
              j += 1;
          }
      }
      
      if (i <= list1.size()) {
          for (int k = i; k < list1.size(); k++) {
              result.add(list1.get(k));
          }
      }
      
      if (j <= list2.size()) {
          for (int k = j; k < list2.size(); k++) {
              result.add(list2.get(k));
          }
      }
      return result;
   }


 /**
   * Return the minimum distance between word1 and word2. Words that are next to each other have a distance of 0.
   * Words that have one word between them have a distance of 1, etc.  Words may appear numerous times in the input
   * paragraph.  This method should return the smallest of those distances.
   *
   * @param word1 First word
   * @param word2 Second word
   * @return An integer representing the minimum distance between word1 and word2
   */
  public int minDistance(String word1, String word2) {
      List<Integer> pos1 = position.get(word1.toLowerCase());
      List<Integer> pos2 = position.get(word2.toLowerCase());
      if (pos1 == null || pos2 == null) {
          throw new IllegalArgumentException();
      }
    //   System.out.printf("%s %s\n", word1, pos1);
    //   System.out.printf("%s %s\n", word2, pos2);
      List<Integer> merged = merge(pos1, pos2);
    //   System.out.printf("%s,%s -> %s\n", word1, word2, merged);
      int bestDist = Integer.MAX_VALUE;
      for (int i = 1; i < merged.size(); i++) {
          int a = merged.get(i-1);
          int b = merged.get(i);
          if (b - a  < bestDist) {
              if (pos1.contains(b) && pos2.contains(a)) {
                  bestDist = b - a;
              } else if (pos1.contains(a) && pos2.contains(b)) {
                  bestDist = b - a;
              }
          }
      }
      
      return bestDist - 1;
  }

 

   
  
  public static void main(String args[] ) throws Exception {
    Scanner scanner = new Scanner(System.in);
    WordDistance solution = new WordDistance();

    StringBuilder results = new StringBuilder();

    String paragraph = scanner.nextLine();
    solution.input(paragraph);

    int numPairsForMinDistance = Integer.parseInt(scanner.nextLine());
    for (int i=0; i < numPairsForMinDistance; i++){
      String nextLine = scanner.nextLine();
      String[] words = nextLine.split(" ");
      results.append(solution.minDistance(words[0],words[1])).append("\n");
    }

    System.out.println(results.toString());
    }
}