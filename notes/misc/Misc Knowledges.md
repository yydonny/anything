
## Security

### RFD (Reflective File Download) attack

Reflective - means user input (query string or URL path param) reflected in response: possible to forge the response body to contain malicious script or command.

File - ability to trick the browser to set the downloaded file's name using the one specified in URL https://example.com/api;/setup.bat;/setup.bat

Download - some MIME type commonly used in REST APIs may cause browser to download the response body as a file. RFC defines the `Content-Disposition` header that can instruct browsers to save a response into a file. This header is highly adopted by Web APIs to prevent XSS attacks as **it forces the response to download instead of being rendered**. However, a common implementation error could result in Reflected File Download from the worst kind. `Content-Disposition` headers **SHOULD always include a "filename" parameter**, to avoid having the browser parse the filename from the URL.


## Random

Committing large transaction will impact entire db performance as WAL has to be synchronously updated. If it drags too long, many other transactions will have wait and the back log could overflow.

## Hadoop Cluster setup

1. hadoop install path must be consistent with $HADOOP_PREFIX set in the master
2. edit hadoop-env.sh file and set JAVA_HOME for Hadoop
3. ensure ssh direct sign in

Typically one machine in the cluster is designated as the NameNode and another machine the as ResourceManager, exclusively. These are the masters.

The rest of the machines in the cluster act as both DataNode and NodeManager. These are the slaves.

## Docker

### Dockerfile: `RUN`, `CMD` and `ENTRYPOINT`

Unlike `CMD`, `ENTRYPOINT` will not be overriden by the user command in `docker run`. Rather, it can be overriden by the `--entrypoint` parameter.

### Networking: Host and Bridge

- `docker run --net=bridge`

Docker creates a **bridge** NIC named `docker0` by default. Both the host and the containers have an IP address on that bridge.

on the Docker host, type sudo `ip addr show docker0` you see the details of that bridge network. Now if you run `route` inside a bridged container, you should see the same IP being displayed as gateway.

- `docker run --net=host`

Alternatively you can run a container with network set to **host**. Such a container will share the network stack with the docker host and from the container point of view, localhost (or 127.0.0.1) will refer to the docker host. Be aware that *any port opened in your container would be opened on the docker host*.


Java collections: LinkedHashMap differs from HashMap in that it maintains a doubly-linked list running through all of its entries. This linked list defines the iteration ordering, which is normally the order in which keys were inserted into the map (insertion-order).
